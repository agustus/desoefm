<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class User extends Main{
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:25:49
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_user"));

		$this->load->library(['pagination','form_validation']);	
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "user";
		$this->global_data['title'] = "Users";
		$this->global_data['description'] = "List Users";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Users',
			'link'	=> site_url('core/user')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'List Users',
			'link'	=> ''
		);

		// Pengaturan pagination
		$config['base_url'] = site_url('core/user/index');
		$config['total_rows'] = count($this->m_user->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$user = $this->m_user->getAllPer($config['per_page'], $id);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($user as $result) {
			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'nama'			=> $result['name'],
				'email'			=> $result['email'],
				'foto'			=> gravatar($result['email']),
				'username'		=> $result['username'],
				'date_created'	=> tgl_indo(substr($result['date_created'], 0, 10)),
				'href_edit'		=> site_url('core/user/edit/'.$result['id'])
			);
			$no++;
		}

		$this->tampilan('user/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "user";
		$this->global_data['title'] = "Add User";
		$this->global_data['description'] = "Add user";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Pengguna',
			'link'	=> site_url('core/user')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Add User',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[12]|is_unique[user.username]', array(
			'required'	=> 'You have not provided %s.',
			'is_unique'	=> 'This %s already exists.'
        ));

		$this->form_validation->set_rules('pass', 'Password', 'trim|required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[pass]');
		$this->form_validation->set_rules('email', 'Email', 'valid_email|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('about', 'About', 'trim|max_length[200]');

        if($this->form_validation->run()){
        	$username = $this->input->post('username');
        	$password = $this->input->post('pass');
        	$nama = $this->input->post('nama');
        	$email = $this->input->post('email');
        	$about = $this->input->post('about');

        	$field = array(
        		'name'			=> $nama,
        		'about'			=> $about,
        		'email' 		=> $email,
        		'username'		=> $username,
        		'password'		=> md5($password),
        		// 'password'		=> crypt($password, KEY_PASS),
        		'date_created' 	=> date('Y-m-d h:i:s')
        	);

        	$tambah = $this->m_user->add($field);

        	if($tambah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah pengguna.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/user');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/user/add');
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('user/form');
	}

	public function edit($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "user";
		$this->global_data['title'] = "Edit User";
		$this->global_data['description'] = "Edit user";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> User',
			'link'	=> site_url('core/user')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Edit User',
			'link'	=> ''
		);

		$this->global_data['datana'] = $this->m_user->getOne(['id'=>$id]);

		if(empty($this->global_data['datana'])){
			redirect('core/user');
		}
		
		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[12]', array(
			'required'	=> 'You have not provided %s.'
        ));

		$this->form_validation->set_rules('pass', 'Password', 'trim');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|matches[pass]');
		$this->form_validation->set_rules('email', 'Email', 'valid_email|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('about', 'About', 'trim|max_length[200]');
		

        if($this->form_validation->run()){
        	$username = $this->input->post('username');
        	$password = $this->input->post('pass');
        	$nama = $this->input->post('nama');
        	$email = $this->input->post('email');
        	$about = $this->input->post('about');

        	$field = array(
        		'name'			=> $nama,
        		'about'			=> $about,
        		'email' 		=> $email,
        		'username'		=> $username,
        		'password'		=> (!empty($password)) ? md5($password) : $this->global_data['datana']['password'],
        		// 'password'		=> (!empty($password)) ? crypt($password, KEY_PASS) : $this->global_data['datana']['password']
        	);

        	$ubah = $this->m_user->change($id,$field);
        	if($ubah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil merubah pengguna.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/user');
        	}else{
        		$this->session->set_flashdata('message','Gagal. Kesalahan database.');
        		redirect('core/user/edit/'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('user/form');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_user->getOne(array('id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');

		if(!empty($id)){
			$hapus = $this->m_user->delete($id);

			if($hapus){
				$response = array('status'=>true, 'message'=>'Berhasil menghapus pengguna.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}
}
?>