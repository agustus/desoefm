<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Profile extends Main{

	/**
		* @Author				: ferdhika
		* @Filename 			: Profile.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-07-09 12:39:09
	**/

	function __construct(){
		parent::__construct();

		$this->load->model("m_user");

		$this->load->library(['form_validation']);	

		$this->global_data['notif'] = $this->session->flashdata('notif');
	}

	public function index(){
		// Identitas halaman
		$this->global_data['active_menu'] = "profile";
		$this->global_data['title'] = "Profile";
		$this->global_data['description'] = "Profile user";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> User',
			'link'	=> site_url('core/profile')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Me',
			'link'	=> site_url('core/profile')
		);

		if($this->input->post('ubahInfo')){
			// Validasi form
			$config = array(
				array(
					'field' => 'nama',
					'label' => 'Full Name',
					'rules' => 'required|min_length[3]|max_length[45]'
				),
				array(
					'field' => 'about',
					'label'	=> 'About',
					'rules' => 'required|min_length[3]|max_length[100]'
				),
				array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'required|valid_email'
				)
			);

			$this->form_validation->set_rules($config);

			if($this->form_validation->run()){
				$nama = $this->input->post('nama');
				$about = $this->input->post('about');
				$email = $this->input->post('email');

				$data = array(
					'name'			=> $nama,
					'about'			=> $about,
					'email'			=> $email
				);

				$edit = $this->m_user->change($this->global_data['akunInfo']['id'], $data);

				if($edit){
					$notif = "<div class=\"alert alert-success alert-dismissable\">";
					$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
					$notif .= "<p>";
					$notif .= "<i class=\"fa fa-check\"></i> Sucessfully edited!";
		         	$notif .= "</p>";
		         	$notif .= "</div>";
				}else{
					$notif = "<div class=\"alert alert-warning alert-dismissable\">";
					$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
					$notif .= "<p>";
					$notif .= "<i class=\"fa fa-warning\"></i> Failed!";
		         	$notif .= "</p>";
		         	$notif .= "</div>";
				}

	         	$this->session->set_flashdata('notif',$notif);

	         	redirect('core/profile');
			}else{
				$notif = "<div class=\"alert alert-danger alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-warning\"></i> ".validation_errors();
	         	$notif .= "</p>";
	         	$notif .= "</div>";

	         	$this->global_data['notif'] = (validation_errors()) ? $notif : $this->session->flashdata('notif');
			}
		}elseif ($this->input->post('ubahPassword')) {
			// Validasi form
			$config = array(
				array(
					'field' => 'password_lama',
					'label' => 'Old Password',
					'rules' => 'required|min_length[6]|max_length[20]|callback_password_exists',
					'errors' => array(
						'required' => 'You must provide a %s.'
					),
				),
				array(
					'field' => 'password_baru',
					'label' => 'New Password',
					'rules' => 'required|min_length[6]|max_length[20]',
					'errors' => array(
						'required' => 'You must provide a %s.',
					),
				),
			);

			$this->form_validation->set_rules($config);

			if($this->form_validation->run()){
				$password = $this->input->post('password_baru');

				$data = array(
					'password'		=> md5($password)
				);

				$edit = $this->m_user->change($this->global_data['akunInfo']['id'], $data);

				if($edit){
					$notif = "<div class=\"alert alert-success alert-dismissable\">";
					$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
					$notif .= "<p>";
					$notif .= "<i class=\"fa fa-check\"></i> Password sucessfully changed!";
		         	$notif .= "</p>";
		         	$notif .= "</div>";
				}else{
					$notif = "<div class=\"alert alert-warning alert-dismissable\">";
					$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
					$notif .= "<p>";
					$notif .= "<i class=\"fa fa-warning\"></i> Failed!";
		         	$notif .= "</p>";
		         	$notif .= "</div>";
				}

	         	$this->session->set_flashdata('notif',$notif);

	         	redirect('core/profile');
			}else{
				$notif = "<div class=\"alert alert-danger alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-warning\"></i> ".validation_errors();
	         	$notif .= "</p>";
	         	$notif .= "</div>";

	         	$this->global_data['notif'] = (validation_errors()) ? $notif : $this->session->flashdata('notif');
			}
		}

		$this->tampilan('profile');
	}

	public function password_exists($str){
		$test = array(
			'username'	=> $this->session->userdata('uname'),
			'password'	=> $str
		);

		$login = $this->m_auth->masuk($test);

		if (empty($login)){
			$this->form_validation->set_message('password_exists', 'The {field} wrong! field can not be the same.');
			return FALSE;
		}else{
			return TRUE;
		}
	}

}