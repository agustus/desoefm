<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Banner extends Main{

	/**
		* @Author				: ferdhika
		* @Filename 			: Banner.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-25 18:14:00
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_banner"));

		$this->load->library(['pagination','form_validation']);	
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "banner";
		$this->global_data['title'] = "Banner";
		$this->global_data['description'] = "Daftar Banner";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-picture-o"></i> Banner',
			'link'	=> site_url('core/banner')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Daftar Banner',
			'link'	=> ''
		);

		// Pengaturan pagination
		$config['base_url'] = site_url('core/banner/index');
		$config['total_rows'] = count($this->m_banner->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$music = $this->m_banner->getAllPer($config['per_page'], $id);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($music as $result) {
			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'banner'		=> (!empty($result['photo_banner'])) ? base_url("assets/upload/".$result['photo_banner']) : base_url("assets/upload/default_banner.jpg"),
				'title'			=> $result['title_banner'],
				'status'		=> ($result['aktif']==1)?'Ditampilkan':'Disembunyikan',
				'href_edit'		=> site_url('core/banner/edit/'.$result['id'])
			);
			$no++;
		}

		$this->tampilan('banner/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "banner";
		$this->global_data['title'] = "Tambah Banner";
		$this->global_data['description'] = "Tambah Banner";
		$this->global_data['datana'] = array();

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-star"></i> Banner',
			'link'	=> site_url('core/banner')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Tambah Banner',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required');

        if($this->form_validation->run()){
        	$data['judul'] = $this->input->post('judul');
	    	$data['description'] = $this->input->post('description');
	    	$data['banner'] = $this->input->post('banner');
	    	$data['status'] = $this->input->post('status');

        	$field = array(
        		'title_banner'		=> $data['judul'],
        		'deskripsi_banner'	=> $data['description'],
        		'photo_banner'		=> $data['banner'],
        		'aktif'				=> $data['status'],
        		'date_created' 		=> date('Y-m-d H:i:s')
        	);

        	$aksi = $this->m_banner->add($field);

        	if($aksi){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah Theme.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/banner');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/banner/add');
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('banner/form');
	}

	public function edit($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "banner";
		$this->global_data['title'] = "Ubah Banner";
		$this->global_data['description'] = "Ubah Banner";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-photo-o"></i> Banner',
			'link'	=> site_url('core/banner')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Ubah Banner',
			'link'	=> ''
		);

		$this->global_data['datana'] = $this->m_banner->getOne(['id'=>$id]);

		if(empty($this->global_data['datana'])){
			redirect('core/banner');
		}
		
		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('judul', 'Judul', 'trim|required');
		$this->form_validation->set_rules('description', 'Deskripsi', 'trim|required');

        if($this->form_validation->run()){
        	$data['judul'] = $this->input->post('judul');
	    	$data['description'] = $this->input->post('description');
	    	$data['banner'] = $this->input->post('banner');
	    	$data['status'] = $this->input->post('status');

        	$field = array(
        		'title_banner'		=> $data['judul'],
        		'deskripsi_banner'	=> $data['description'],
        		'photo_banner'		=> $data['banner'],
        		'aktif'				=> $data['status'],
        	);

        	$ubah = $this->m_banner->edit($field,['id'=>$id]);
        	if($ubah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil merubah Banner.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/banner');
        	}else{
        		$this->session->set_flashdata('message','Gagal. Kesalahan database.');
        		redirect('core/banner/edit/'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('banner/form');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_banner->getOne(array('id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');
		

		if(!empty($id)){
			$hapus = $this->m_banner->remove(['id'=>$id]);

			if($hapus){
				$response = array('status'=>true, 'message'=>'Berhasil menghapus Banner.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}
}