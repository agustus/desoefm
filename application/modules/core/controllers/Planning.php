<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Planning extends Main{

	/**
		* @Author				: ferdhika
		* @Filename 			: Planning.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-07-31 21:21:38
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_planning"));

		$this->load->library(['pagination','form_validation']);	
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "planning";
		$this->global_data['title'] = "Planning";
		$this->global_data['description'] = "List Planning";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-podcast"></i> Planning',
			'link'	=> site_url('core/program')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'List Planning',
			'link'	=> ''
		);

		// Pengaturan pagination
		$config['base_url'] = site_url('core/planning/index');
		$config['total_rows'] = count($this->m_planning->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$planning = $this->m_planning->getAllPer($config['per_page'], $id);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($planning as $result) {
			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'name'			=> $result['planning_name'],
				'date'			=> $result['planning_date'],
				'time'			=> substr($result['planning_time'], 0, 5),
				'status'		=> $result['planning_status'],
				'date_created'	=> tgl_indo(substr($result['created_at'], 0, 10)),
				'href_edit'		=> site_url('core/planning/edit/'.$result['id'])
			);
			$no++;
		}

		$this->tampilan('planning/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "planning";
		$this->global_data['title'] = "Add Planning";
		$this->global_data['description'] = "Add planning";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-podcast"></i> Planning',
			'link'	=> site_url('core/planning')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Add Planning',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi
		$this->form_validation->set_rules('name', 'Planning Name', 'required');
		$this->form_validation->set_rules('description', 'Planning Description', 'required');
		$this->form_validation->set_rules('date', 'Planning Date', 'required|date');
		$this->form_validation->set_rules('time', 'Planning Time', 'required');

        if($this->form_validation->run()){
        	$name = $this->input->post('name');
        	$description = $this->input->post('description');
        	$date = $this->input->post('date');
        	$time = $this->input->post('time');

        	$field = array(
        		'planning_name'			=> $name,
        		'planning_description'	=> $description,
        		'planning_date' 		=> $date,
        		'planning_time'			=> $time,
        		'created_at'	 		=> date('Y-m-d h:i:s')
        	);

        	$tambah = $this->m_planning->add($field);

        	if($tambah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah planning.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/planning');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/planning/add');
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->global_data['script'] = [

		];

		$this->tampilan('planning/form');
	}

	public function edit($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "planning";
		$this->global_data['title'] = "Edit Planning";
		$this->global_data['description'] = "Edit planning";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-podcast"></i> Planning',
			'link'	=> site_url('core/planning')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Edit Planning',
			'link'	=> ''
		);

		$this->global_data['datana'] = $this->m_planning->getOne(['id'=>$id]);

		if(empty($this->global_data['datana'])){
			redirect('core/planning');
		}
		
		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi
		// $this->form_validation->set_rules('name', 'Username', 'required|min_length[4]|max_length[12]', array(
		// 	'required'	=> 'You have not provided %s.'
  //       ));

		$this->form_validation->set_rules('name', 'Planning Name', 'required');
		$this->form_validation->set_rules('description', 'Planning Description', 'required');
		$this->form_validation->set_rules('date', 'Planning Date', 'required|date');
		$this->form_validation->set_rules('time', 'Planning Time', 'required');
		

        if($this->form_validation->run()){
        	$name = $this->input->post('name');
        	$description = $this->input->post('description');
        	$date = $this->input->post('date');
        	$time = $this->input->post('time');

        	$field = array(
        		'planning_name'			=> $name,
        		'planning_description'	=> $description,
        		'planning_date' 		=> $date,
        		'planning_time'			=> $time
        	);

        	$ubah = $this->m_planning->change($id,$field);
        	if($ubah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil merubah planning.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/planning');
        	}else{
        		$this->session->set_flashdata('message','Gagal. Kesalahan database.');
        		redirect('core/planning/edit/'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('planning/form');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_planning->getOne(array('id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');

		if(!empty($id)){
			$hapus = $this->m_planning->delete($id);

			if($hapus){
				$response = array('status'=>true, 'message'=>'Berhasil menghapus planning.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}

}