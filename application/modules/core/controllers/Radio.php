<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Radio extends Main{

	/**
		* @Author				: ferdhika
		* @Filename 			: Radio.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-25 13:50:24
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_page"));

		$this->load->library(['pagination','form_validation']);	

		$this->global_data['notif'] = $this->session->flashdata('notif');
	}

	public function index(){
		// Identitas halaman
		$this->global_data['active_menu'] = "radio";
		$this->global_data['title'] = "The Radio";
		$this->global_data['description'] = "Eltras Radio Politeknik Negeri Bandung";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-briefcase"></i> Halaman',
			'link'	=> site_url('core/news')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Ubah Halaman',
			'link'	=> ''
		);

		$this->global_data['script_up'] = array(
			base_url('assets/tinymce/tinymce.min.js')
		);

		// Validasi form
		$config = array(
			array(
				'field' => 'title',
				'label' => 'Article Title',
				'rules' => 'required|min_length[3]|max_length[80]'
			),
			array(
				'field' => 'content',
				'label'	=> 'Article Content',
				'rules' => 'required|min_length[25]',
				'errors' => array(
					'required' => 'You must provide a %s.',
				),
			)
		);

		$this->form_validation->set_rules($config);

		$cek = $this->m_page->getOne(array('type'=>'radio'));

		$this->global_data['datana'] = $cek;

		if($this->form_validation->run()){
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$banner = $this->input->post('banner');

			$data = array(
				'nama_page'		=> $title,
				'content_page'	=> $content,
				'banner'		=> $banner,
				'type'			=> 'radio'
			);

			if(empty($cek)){
				$data['date_created'] = date('Y-m-d H:i:s');
				$aksi = $this->m_page->add($data);
			}else{
				$data['date_updated'] = date('Y-m-d H:i:s');
				$aksi = $this->m_page->edit($data, array('type'=>'radio'));
			}

			if($aksi){
				$notif = "<div class=\"alert alert-success alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-check\"></i> Sucessfully edited!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}else{
				$notif = "<div class=\"alert alert-warning alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-warning\"></i> Failed!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}

         	$this->session->set_flashdata('notif',$notif);
         	redirect('core/radio');
		}

		$this->tampilan('radio/form');
	}

}