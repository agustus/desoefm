<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class News extends Main{

	/**
		* @Author				: Ferdhika Yudira
		* @Filename 			: News.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-06 15:40:52
	**/

	function __construct(){
		parent::__construct();

		$this->load->model("m_news");

		$this->load->library(['pagination','form_validation']);	

		$this->global_data['notif'] = $this->session->flashdata('notif');
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "news";
		$this->global_data['title'] = "News";
		$this->global_data['description'] = "List news";
		$this->global_data['activeList'] = true;

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-newspaper-o"></i> News',
			'link'	=> site_url('core/news')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'List News',
			'link'	=> site_url('core/news')
		);

		// Link
		$this->global_data['href_get_one']	= site_url().'/core/news/getOne';
		$this->global_data['href_hapus']	= site_url().'/core/news/hapus';

		// Pengaturan pagination
		$config['base_url'] = site_url().'core/news/index';
		$config['total_rows'] = count($this->m_news->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$datana = $this->m_news->getAllPer($config['per_page'], $id);

		$this->global_data['btn_add'] = 'Add News';
		$this->global_data['href_add'] = site_url('core/news/add');

		$this->global_data['hasilData'] = array();

		$no=1+$id;
		foreach ($datana as $result) {
			$this->global_data['hasilData'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'title'			=> $result['title'],
				'status'		=> ($result['status']==1) ? "Published" : "Draf",
				'date_created'	=> substr($result['date_created'], 0, 10),					
				'href_edit'		=> site_url('core/news/edit/'.$result['id'])
			);
			$no++;
		}

		$this->tampilan('news/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "news";
		$this->global_data['title'] = "Create News";
		$this->global_data['description'] = "Create news";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-newspaper-o"></i> News',
			'link'	=> site_url('core/news')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Create News',
			'link'	=> ''
		);

		// Validasi form
		$config = array(
			array(
				'field' => 'title',
				'label' => 'Article Title',
				'rules' => 'required|min_length[3]|max_length[80]'
			),
			array(
				'field' => 'content',
				'label'	=> 'Article Content',
				'rules' => 'required|min_length[25]',
				'errors' => array(
					'required' => 'You must provide a %s.',
				),
			)
		);

		$this->form_validation->set_rules($config);

		if($this->form_validation->run()){
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$status = $this->input->post('status');

			$data = array(
				'title'		=> $title,
				'content'	=> $content,
				'status'	=> $status,
				'user_id'	=> $this->session->userdata('id')
			);

			$add = $this->m_news->add($data);

			if($add){
				$notif = "<div class=\"alert alert-success alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-check\"></i> Sucessfully added!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}else{
				$notif = "<div class=\"alert alert-warning alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-warning\"></i> Failed!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}

         	$this->session->set_flashdata('notif',$notif);
         	redirect('core/news');
		}else{
			$notif = "<div class=\"alert alert-danger alert-dismissable\">";
			$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
			$notif .= "<p>";
			$notif .= "<i class=\"fa fa-warning\"></i> ".validation_errors();
         	$notif .= "</p>";
         	$notif .= "</div>";

         	$this->global_data['notif'] = (validation_errors()) ? $notif : $this->session->flashdata('notif');
		}

		$this->form();
	}

	public function edit($id=0){
		(empty($id)) ? redirect('core/news') : '';

		// Identitas halaman
		$this->global_data['active_menu'] = "news";
		$this->global_data['title'] = "Edit News";
		$this->global_data['description'] = "Edit news";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-newspaper-o"></i> News',
			'link'	=> site_url('core/news')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Edit News',
			'link'	=> ''
		);

		$this->global_data['datana'] = $this->m_news->getOne(array('id'=>$id));

		(empty($this->global_data['datana'])) ? redirect('core/news') : '';

		// Validasi form
		$config = array(
			array(
				'field' => 'title',
				'label' => 'News Title',
				'rules' => 'required|min_length[3]|max_length[80]'
			),
			array(
				'field' => 'content',
				'label'	=> 'News Content',
				'rules' => 'required|min_length[25]',
				'errors' => array(
					'required' => 'You must provide a %s.',
				),
			)
		);

		$this->form_validation->set_rules($config);

		if($this->form_validation->run()){
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$status = $this->input->post('status');

			$data = array(
				'title'		=> $title,
				'content'	=> $content,
				'status'	=> $status
			);

			$edit = $this->m_news->edit($data,array('id'=> $id));

			if($edit){
				$notif = "<div class=\"alert alert-success alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-check\"></i> Sucessfully edited!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}else{
				$notif = "<div class=\"alert alert-warning alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-warning\"></i> Failed!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}

         	$this->session->set_flashdata('notif',$notif);
         	redirect('core/news');
		}else{
			$notif = "<div class=\"alert alert-danger alert-dismissable\">";
			$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
			$notif .= "<p>";
			$notif .= "<i class=\"fa fa-warning\"></i> ".validation_errors();
         	$notif .= "</p>";
         	$notif .= "</div>";

         	$this->global_data['notif'] = (validation_errors()) ? $notif : $this->session->flashdata('notif');
		}

		$this->form();
	}

	private function form(){
		$this->global_data['script_up'] = array(
			base_url('assets/tinymce/tinymce.min.js')
		);

		$this->tampilan('news/form');
	}

	public function getOne($id=0){
		(empty($id)) ? redirect('core/news') : '';

		$data = $this->m_news->getOne(array('id'=>$id));

		$this->outputJson($data);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');

		if(!empty($id)){
			
			$hapus = $this->m_news->remove(array('id' => $id));

			if($hapus){
				$response = array('status'=>true, 'message'=>'Sucessfully deleted.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
			
		}

		$this->outputJson($response);
	}
}