<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Chart extends Main{
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:25:49
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_chart","m_music"));

		$this->load->library(['pagination','form_validation']);	
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "chart";
		$this->global_data['title'] = "Chart";
		$this->global_data['description'] = "Daftar Chart";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Chart',
			'link'	=> site_url('core/bar-chart')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Daftar Chart',
			'link'	=> ''
		);

		// Pengaturan pagination
		$config['base_url'] = site_url('core/chart/index');
		$config['total_rows'] = count($this->m_chart->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$chart = $this->m_chart->getAllPer($config['per_page'], $id);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($chart as $result) {
			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'title'			=> $result['title'],
				'artist'		=> $result['artist'],
				'position'		=> $result['position'],
				'date_created'	=> tgl_indo(substr($result['date_created'], 0, 10)),
				'href_edit'		=> site_url('core/chart/edit/'.$result['id'])
			);
			$no++;
		}

		$this->tampilan('chart/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "chart";
		$this->global_data['title'] = "Tambah Chart";
		$this->global_data['description'] = "Tambah Chart";
		$this->global_data['datana'] = array();

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Chart',
			'link'	=> site_url('core/chart')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Tambah Chart',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		//Data
		$this->global_data['music'] = $this->m_music->getAll();
		// Validasi

		$this->form_validation->set_rules('music', 'Music', 'trim|required');
		$this->form_validation->set_rules('position', 'Posisi', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

    	$data['music'] = $this->input->post('music');
    	$data['position'] = $this->input->post('position');
    	$data['status'] = $this->input->post('status');

        if($this->form_validation->run()){

    		$field = array(
        		'music_id'		=> $data['music'],
        		'position'		=> $data['position'],
        		'isShow'		=> $data['status'],
        		'date_created' 	=> date('Y-m-d h:i:s'),
        		'date_updated' 	=> date('Y-m-d h:i:s')
        	);

    		$tambah = $this->m_chart->add($field);

        	if($tambah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah Chart.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/chart');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/chart/add');
        	}
        }else{
			// Pesan validasi
			$this->global_data['datana'] = $data;
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('chart/form');
	}

	public function edit($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "chart";
		$this->global_data['title'] = "Ubah Chart";
		$this->global_data['description'] = "Ubah Chart";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Chart',
			'link'	=> site_url('core/chart')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Ubah Chart',
			'link'	=> ''
		);
		//Data
		$this->global_data['music'] = $this->m_music->getAll();

		$datana = $this->m_chart->getOne(['top_chart.id'=>$id]);
		$datana['music'] = $datana['music_id'];
		$datana['status'] = $datana['isShow'];
		$this->global_data['datana'] = $datana;

		if(empty($this->global_data['datana'])){
			redirect('core/chart');
		}
		
		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('position', 'Posisi', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');


        if($this->form_validation->run()){
        	
        	$data['music'] = $this->input->post('music');
	    	$data['position'] = $this->input->post('position');
	    	$data['status'] = $this->input->post('status');

	    	$field = array(
	    			'music_id'		=> $data['music'],
	        		'position'		=> $data['position'],
	        		'isShow'		=> $data['status'],
	        		'date_created' 	=> $cek['date_updated'],
	        		'date_updated' 	=> date('Y-m-d h:i:s')
	        	);

        	$ubah = $this->m_chart->change($id,$field);
        	if($ubah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil merubah Chart.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/chart');
        	}else{
        		$this->session->set_flashdata('message','Gagal. Kesalahan database.');
        		redirect('core/chart/edit/'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('chart/form');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_chart->getOne(array('top_chart.id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');
		
		if(!empty($id)){
			
			$hapus = $this->m_chart->delete($id);
			if($hapus){
				
				$response = array('status'=>true, 'message'=>'Berhasil menghapus Chart.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}

	public function changeOthers($start,$field){

		if($field['position'] < $start){
			$awal = $field['position'];
			$akhir = $start;
			$counter = 1;
		} else {
			$awal = $start;
			$akhir = $field['position']+1;
			$counter = -1;
		}

		for($i=$awal; $i < $akhir; $i++){
			$cek = $this->m_chart->getOne(['position'=>$i, 'isShow'=>true]);
			if($cek){
				$field['position'] = $i + ($counter);
        		$tambah = $this->m_chart->change($cek['id'],$field);
			}
		}
	}

	public function tes(){
		$max = $this->m_chart->getMax();

		print_r($max);
		echo $max[0]['position'];
	}
}
?>