<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Crew extends Main{
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:25:49
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_crew"));

		$this->load->library(['pagination','form_validation']);	
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "crew";
		$this->global_data['title'] = "Crew";
		$this->global_data['description'] = "List Crew";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Crew',
			'link'	=> site_url('core/crew')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'List Crew',
			'link'	=> ''
		);

		// Pengaturan pagination
		$config['base_url'] = site_url('core/crew/index');
		$config['total_rows'] = count($this->m_crew->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$crew = $this->m_crew->getAllPer($config['per_page'], $id);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($crew as $result) {
			$jabatan = $this->m_crew->getOneJabatan(array('id'=>$result['jabatan_id']));

			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'nama'			=> $result['name'],
				'jabatan'		=> $jabatan,
				'description'	=> $result['description'],
				'foto'			=> (!empty($result['photo'])) ? base_url('assets/upload/'.$result['photo']) : base_url("assets/img/anggota/default.png"),
				'isShow'			=> $result['isShow'],
				'date_created'	=> tgl_indo(substr($result['date_created'], 0, 10)),
				'href_edit'		=> site_url('core/crew/edit/'.$result['id'])
			);
			$no++;
		}

		$this->tampilan('crew/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "crew";
		$this->global_data['title'] = "Add Crew";
		$this->global_data['description'] = "Add Crew";
		$this->global_data['datana'] = array();

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Crew',
			'link'	=> site_url('core/crew')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Add Crew',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		//Data
		$this->global_data["data_jabatan"] = $this->m_crew->getJabatan();

		// Validasi
		$this->form_validation->set_rules('name', 'Nama', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Dekripsi', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required');

    	$data['photo'] = $this->input->post('photo');
    	$data['name'] = $this->input->post('name');
    	$data['facebook'] = $this->input->post('facebook');
    	$data['twitter'] = $this->input->post('twitter');
    	$data['linkedin'] = $this->input->post('linkedin');
    	$data['gplus'] = $this->input->post('gplus');
    	$data['description'] = $this->input->post('deskripsi');
    	$data['jabatan_id'] = $this->input->post('jabatan');
    	$data['isShow'] = $this->input->post('status');

        if($this->form_validation->run()){

        	$data['date_created'] = date('Y-m-d h:i:s');

        	$tambah = $this->m_crew->add($data);

        	if($tambah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah Crew.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/crew');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/crew/add');
        	}
        }else{
			// Pesan validasi
			$this->global_data['datana'] = $data;
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('crew/form');
	}

	public function edit($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "crew";
		$this->global_data['title'] = "Edit Crew";
		$this->global_data['description'] = "Edir Crew";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Crew',
			'link'	=> site_url('core/crew')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Edit Crew',
			'link'	=> ''
		);

		$this->global_data["data_jabatan"] = $this->m_crew->getJabatan();

		$data = $this->m_crew->getOne(['crew.id'=>$id]);

		$data['jabatan'] = $data['jabatan_id'];
		$data['status'] = $data['isShow'];
		$this->global_data['datana'] = $data;

		if(empty($this->global_data['datana'])){
			redirect('core/crew');
		}
		
		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('name', 'Nama', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Dekripsi', 'trim|required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

        if($this->form_validation->run()){
        	$field['photo'] = $this->input->post('photo');
	    	$field['name'] = $this->input->post('name');
	    	$field['facebook'] = $this->input->post('facebook');
	    	$field['twitter'] = $this->input->post('twitter');
	    	$field['linkedin'] = $this->input->post('linkedin');
	    	$field['gplus'] = $this->input->post('gplus');
	    	$field['description'] = $this->input->post('deskripsi');
	    	$field['jabatan_id'] = $this->input->post('jabatan');
	    	$field['isShow'] = $this->input->post('status');
	    	$field['date_created'] = date('Y-m-d h:i:s');

        	$ubah = $this->m_crew->change($id,$field);
        	if($ubah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil merubah Crew.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/crew');
        	}else{
        		$this->session->set_flashdata('message','Gagal. Kesalahan database.');
        		redirect('core/crew/edit/'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('crew/form');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_crew->getOne(array('crew.id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');
		

		if(!empty($id)){
			$hapus = $this->m_crew->delete($id);

			if($hapus){
				$response = array('status'=>true, 'message'=>'Berhasil menghapus Crew.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}
}
?>