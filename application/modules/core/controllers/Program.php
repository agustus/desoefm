<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Program extends Main{
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:25:49
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_program"));

		$this->load->library(['form_validation']);	
	}

	public function index(){
		// Identitas halaman
		$this->global_data['active_menu'] = "program";
		$this->global_data['title'] = "Broadcast Schedule";
		$this->global_data['description'] = "List Broadcast Schedule";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-podcast"></i> Broadcast Schedule',
			'link'	=> site_url('core/program')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'List Broadcast Schedule',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$allDay = $this->m_program->getAllday();

		$this->global_data['allDay'] = array();

		foreach ($allDay as $allDay) {
			$program = $this->m_program->getOneday(['id' => $allDay['id']]);

			$this->global_data['allDay'][] = [
				'id'	=> $allDay['id'],
				'day'	=> $allDay['day'],
				'program'=> $this->m_program->getAllperDay($allDay['id'])
			];
		}

		$this->tampilan('program/list');
	}

	public function add($id){
		$this->global_data['day_title'] = $this->m_program->getOneday(['id' => $id, 'status'=>1]);

		if(empty($this->global_data['day_title'])){
			redirect('core/program');
		}

		// Identitas halaman
		$this->global_data['active_menu'] = "program";
		$this->global_data['title'] = "Broadcast Schedule ".$this->global_data['day_title']['day'];
		$this->global_data['description'] = "Add Broadcast Schedule ".$this->global_data['day_title']['day'];

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-podcast"></i> Broadcast Schedule',
			'link'	=> site_url('core/program')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Add Broadcast Schedule',
			'link'	=> ''
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> $this->global_data['day_title']['day'],
			'link'	=> ''
		);


		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi
		$this->form_validation->set_rules('jam', 'Jam', 'trim|required');
		$this->form_validation->set_rules('acara', 'Acara', 'trim|required');

		

    	$data['time'] = $this->input->post('jam');
    	$data['event'] = $this->input->post('acara');

        if($this->form_validation->run()){

        	$field = array(
        		'day_id' 	=> $id,
        		'time'		=> $data['time'],
        		'event'		=> $data['event'],
        		'status'	=> 1
        	);

        	$tambah = $this->m_program->add($field);

        	if($tambah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Sukses!</h4>";
				$pesan .= "		Berhasil menambah Jadwal ".$this->global_data['day_title']['day'];
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/program');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/program/program'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['datana'] = $data;
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('program/form');
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');
		
		$temp = $this->m_program->getOne(array('id'=>$id));
		$temp = $this->m_program->getOneday(array('id'=>$temp['day_id']));
		if(!empty($id)){
			$hapus = $this->m_program->delete($id);

			if($hapus){
				$response = array('status'=>true, 'message'=>'Berhasil menghapus Jadwal '.$temp['day'].".");
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}
}
?>