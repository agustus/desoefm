<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Iklan extends Main{
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:25:49
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_iklan"));

		$this->load->library(['pagination','form_validation']);	
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "iklan";
		$this->global_data['title'] = "Iklan";
		$this->global_data['description'] = "Daftar Iklan";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Iklan',
			'link'	=> site_url('core/iklan')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Daftar Iklan',
			'link'	=> ''
		);

		// Pengaturan pagination
		$config['base_url'] = site_url('core/iklan/index');
		$config['total_rows'] = count($this->m_iklan->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$crew = $this->m_iklan->getAllPer($config['per_page'], $id);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($crew as $result) {
			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'title'			=> $result['title'],
				'url'			=> $result['url'],
				'description'	=> $result['description'],
				'foto'			=> (!empty($result['photo'])) ? base_url('assets/upload/'.$result['photo']) : base_url("assets/upload/iklan/default_iklan.jpg"),
				'position'		=> $result['position'],
				'date_created'	=> tgl_indo(substr($result['date_created'], 0, 10)),
				'href_edit'		=> site_url('core/iklan/edit/'.$result['id'])
			);
			$no++;
		}

		$this->tampilan('iklan/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "iklan";
		$this->global_data['title'] = "Tambah Iklan";
		$this->global_data['description'] = "Tambah Iklan";
		$this->global_data['datana'] = array();

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Iklan',
			'link'	=> site_url('core/iklan')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Tambah iklan',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi

		$this->form_validation->set_rules('title', 'Nama', 'trim|required');
		$this->form_validation->set_rules('url', 'URL', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('position', 'Posisi', 'trim|required');

    	$data['photo'] = $this->input->post('photo');
    	$data['title'] = $this->input->post('title');
    	$data['url'] = $this->input->post('url');
    	$data['position'] = $this->input->post('position');
    	$data['description'] = $this->input->post('deskripsi');

        if($this->form_validation->run()){

        	$field = array(
        		'title'			=> $data['title'],
        		'url'			=> $data['url'],
        		'description'	=> $data['description'],
        		'photo'			=> $data['photo'],
        		'position'		=> $data['position'],
        		'date_created' 	=> date('Y-m-d h:i:s')
        	);

        	$tambah = $this->m_iklan->add($field);

        	if($tambah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah Crew.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/iklan');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/iklan/add');
        	}
        }else{
			// Pesan validasi
			$this->global_data['datana'] = $data;
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('iklan/form');
	}

	public function edit($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "iklan";
		$this->global_data['title'] = "Ubah Iklan";
		$this->global_data['description'] = "Ubah Iklan";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-user"></i> Iklan',
			'link'	=> site_url('core/iklan')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Ubah Crew',
			'link'	=> ''
		);

		$this->global_data['datana'] = $this->m_iklan->getOne(['id'=>$id]);

		if(empty($this->global_data['datana'])){
			redirect('core/iklan');
		}
		
		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('title', 'Nama', 'trim|required');
		$this->form_validation->set_rules('url', 'URL', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('position', 'Posisi', 'trim|required');

        if($this->form_validation->run()){
        	
			$data['photo'] = $this->input->post('photo');
			$data['title'] = $this->input->post('title');
			$data['url'] = $this->input->post('url');
			$data['position'] = $this->input->post('position');
			$data['description'] = $this->input->post('deskripsi');

        	$field = array(
        		'title'			=> $data['title'],
        		'url'			=> $data['url'],
        		'description'	=> $data['description'],
        		'photo'			=> $data['photo'],
        		'position'		=> $data['position']
        	);

        	$ubah = $this->m_iklan->change($id,$field);
        	if($ubah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil merubah Crew.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/iklan');
        	}else{
        		$this->session->set_flashdata('message','Gagal. Kesalahan database.');
        		redirect('core/iklan/edit/'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('iklan/form');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_iklan->getOne(array('id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');
		

		if(!empty($id)){
			$hapus = $this->m_iklan->delete($id);

			if($hapus){
				$response = array('status'=>true, 'message'=>'Berhasil menghapus Crew.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}
}
?>