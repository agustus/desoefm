<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Page extends Main{

	/**
		* @Author				: ferdhika
		* @Filename 			: Page.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-07-28 22:20:12
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_page"));

		$this->load->library(['pagination','form_validation']);	

		$this->global_data['notif'] = $this->session->flashdata('notif');
	}

	public function index($type=null){

		$cek = $this->m_page->getOne(array('type'=>$type));

		if(empty($type) || empty($cek)){
			exit;
		}
		// Identitas halaman
		$this->global_data['active_menu'] = $type;
		$this->global_data['title'] = $cek['nama_page'];
		$this->global_data['description'] = $cek['deskripsi_page'];

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-file"></i> Page',
			'link'	=> site_url('core/page')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Page '.$cek['nama_page'],
			'link'	=> ''
		);

		$this->global_data['script_up'] = array(
			base_url('assets/tinymce/tinymce.min.js')
		);

		// Validasi form
		$config = array(
			array(
				'field' => 'title',
				'label' => 'Page Title',
				'rules' => 'required|min_length[3]|max_length[80]'
			),
			array(
				'field' => 'content',
				'label'	=> 'Page Content',
				'rules' => 'required|min_length[25]',
				'errors' => array(
					'required' => 'You must provide a %s.',
				),
			)
		);

		$this->form_validation->set_rules($config);

		$this->global_data['datana'] = $cek;

		if($this->form_validation->run()){
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$banner = $this->input->post('banner');

			$data = array(
				'nama_page'		=> $title,
				'content_page'	=> $content,
				'banner'		=> $banner,
				'type'			=> $type
			);

			if(empty($cek)){
				$data['date_created'] = date('Y-m-d H:i:s');
				$aksi = $this->m_page->add($data);
			}else{
				$data['date_updated'] = date('Y-m-d H:i:s');
				$aksi = $this->m_page->edit($data, array('type'=>$type));
			}

			if($aksi){
				$notif = "<div class=\"alert alert-success alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-check\"></i> Sucessfully edited!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}else{
				$notif = "<div class=\"alert alert-warning alert-dismissable\">";
				$notif .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$notif .= "<p>";
				$notif .= "<i class=\"fa fa-warning\"></i> Failed!";
	         	$notif .= "</p>";
	         	$notif .= "</div>";
			}

         	$this->session->set_flashdata('notif',$notif);
         	redirect('core/page/'.$type);
		}

		$this->tampilan('page/form');
	}

}