<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Theme extends Main{

	/**
		* @Author				: ferdhika
		* @Filename 			: Theme.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-25 14:43:08
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_theme"));

		$this->load->library(['pagination','form_validation']);	
	}

	public function index($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "theme";
		$this->global_data['title'] = "Theme";
		$this->global_data['description'] = "Daftar Theme";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-star"></i> Theme',
			'link'	=> site_url('core/theme')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Daftar Theme',
			'link'	=> ''
		);

		// Pengaturan pagination
		$config['base_url'] = site_url('core/theme/index');
		$config['total_rows'] = count($this->m_theme->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$music = $this->m_theme->getAllPer($config['per_page'], $id);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($music as $result) {
			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'content'		=> $result['content'],
				'bulan'			=> $result['bulan'],
				'tahun'			=> $result['tahun'],
				'href_edit'		=> site_url('core/theme/edit/'.$result['id'])
			);
			$no++;
		}

		$this->global_data['dataAktif'] = $this->m_theme->getOne(array('bulan'=> date('m'), 'tahun'=> date('Y')));

		$this->tampilan('theme/list');
	}

	public function add(){
		// Identitas halaman
		$this->global_data['active_menu'] = "theme";
		$this->global_data['title'] = "Tambah Theme";
		$this->global_data['description'] = "Tambah Theme";
		$this->global_data['datana'] = array();

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-star"></i> Theme',
			'link'	=> site_url('core/theme')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Tambah Theme',
			'link'	=> ''
		);

		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		// Validasi
		$this->form_validation->set_rules('content', 'Konten', 'trim|required');

        if($this->form_validation->run()){
        	$data['content'] = $this->input->post('content');
	    	$data['bulan'] = $this->input->post('bulan');
	    	$data['tahun'] = $this->input->post('tahun');

        	$field = array(
        		'content'		=> $data['content'],
        		'bulan'			=> $data['bulan'],
        		'tahun'			=> $data['tahun'],
        		'date_created' 	=> date('Y-m-d H:i:s')
        	);

        	$cek = $this->m_theme->getOne(array('bulan'=> $data['bulan'], 'tahun'=> $data['tahun']));

        	if(empty($cek)){
        		$aksi = $this->m_theme->add($field);
        	}else{
        		$aksi = $this->m_theme->edit($field, array('id'=> $cek['id']));
        	}

        	if($aksi){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah Theme.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/theme');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('core/theme/add');
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('theme/form');
	}

	public function edit($id=0){
		// Identitas halaman
		$this->global_data['active_menu'] = "theme";
		$this->global_data['title'] = "Ubah Theme";
		$this->global_data['description'] = "Ubah Theme";

		// Breadcumb
		$this->global_data['breadcumb'][] = array(
			'judul'	=> '<i class="fa fa-star"></i> Theme',
			'link'	=> site_url('core/theme')
		);
		$this->global_data['breadcumb'][] = array(
			'judul'	=> 'Ubah Theme',
			'link'	=> ''
		);

		$this->global_data['datana'] = $this->m_theme->getOne(['id'=>$id]);

		if(empty($this->global_data['datana'])){
			redirect('core/theme');
		}
		
		// Pesan
		$this->global_data['message'] = $this->session->flashdata('message');

		$this->form_validation->set_rules('content', 'Konten', 'trim|required');

        if($this->form_validation->run()){
        	$data['content'] = $this->input->post('content');
	    	$data['bulan'] = $this->input->post('bulan');
	    	$data['tahun'] = $this->input->post('tahun');

        	$field = array(
        		'content'		=> $data['content'],
        		'bulan'			=> $data['bulan'],
        		'tahun'			=> $data['tahun']
        	);

        	$ubah = $this->m_theme->edit($field,['id'=>$id]);
        	if($ubah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil merubah Theme.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('core/theme');
        	}else{
        		$this->session->set_flashdata('message','Gagal. Kesalahan database.');
        		redirect('core/theme/edit/'.$id);
        	}
        }else{
			// Pesan validasi
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('theme/form');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_theme->getOne(array('id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}

	public function hapus(){
		$response = array('status'=>false, 'message'=>null);

		@$id=$this->input->post('id');
		

		if(!empty($id)){
			$hapus = $this->m_theme->remove(['id'=>$id]);

			if($hapus){
				$response = array('status'=>true, 'message'=>'Berhasil menghapus Theme.');
			}else{
				$response = array('status'=>false, 'message'=>'Kesalahan database');
			}
		}

		$this->outputJson($response);
	}
}