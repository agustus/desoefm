<!-- Content Wrapper. Contains page content -->

	<div class="content-wrapper">

		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                if(!empty($breadcumb)):
            ?>
                <ol class="breadcrumb">
            <?php
                    foreach ($breadcumb as $breadcumb):
                        if(empty($breadcumb['link'])):
            ?>
                            <li class="active"><?php echo $breadcumb['judul'];?></li>
            <?php
                        else:
            ?>
                            <li>
                                <a href="<?php echo $breadcumb['link'];?>">
                                    <?php echo $breadcumb['judul'];?>
                                </a>
                            </li>
            <?php
                        endif;
                    endforeach;
            ?>
                </ol>
            <?php
                endif;
            ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div id="alert"></div>
					<?php if(!empty($message)): ?>
						<?php echo $message;?>
					<?php endif;?>

					<a class="btn btn-primary btn-md" href="<?php echo site_url('core/user/add');?>">
						<i class="fa fa-plus"></i> Tambah <?php echo $title;?>
					</a> <br></br>
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">List <?php echo $title;?></h3>
						</div><!-- /.box-header -->
						<div class="box-body">
							<table class="table table-bordered">
								<tr>
									<th style="width: 20px">#</th>
									<th>Foto</th>
									<th>Nama</th>
									<th>Email</th>
									<th>Tanggal Dibuat</th>
									<th style="width: 10%">Aksi</th>
								</tr>
								<?php
									if(!empty($data)):
										foreach ($data as $data):
								?>
								<tr id="histori<?php echo $data['id'];?>">
									<td>
										<?php echo $data['no'];?>
									</td>
									<td style="text-align:center;cursor:pointer;">
										<img onclick="lihatFoto('<?php echo $data['foto'];?>')" src="<?php echo $data['foto'];?>" height="50px">
									</td>
									<td>
										<?php echo $data['nama'];?>
									</td>
									<td>
										<?php echo $data['email'];?>
									</td>
									<td>
										<?php echo $data['date_created'];?>
									</td>
									<td style="width: 20%">
										<button class="btn btn-xs btn-primary" onclick="view(<?php echo $data['id'];?>)" title="Lihat">
											<i class="fa fa-eye"></i>
										</button>
										<a class="btn btn-default btn-xs" href="<?php echo $data['href_edit'];?>" title="Ubah">
											<i class="fa fa-pencil"></i>
										</a>
										<button class="btn btn-danger btn-xs" onclick="mdlhapus(<?php echo $data['id'];?>)" title="Hapus">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php
										endforeach;
									else:
								?>
								<tr>
									<td colspan="7">Tidak ada data.</td>
								</tr>
								<?php
									endif;
								?>
							</table>
						</div><!-- /.box-body -->
						<div class="box-footer clearfix">
							<?php echo (!empty($halaman)) ? $halaman:'';?>
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

<script type="text/javascript">
	function lihatFoto(url){
		$("#fotona").attr("src", url);
		$('#modalFoto').modal('show'); // show bootstrap modal
	}

	function view(obj){
		var id = obj;

		$.ajax({
			url:"<?php echo site_url('core/user/ambilSatu')?>/"+id,
			type:'get',
			dataType: 'json',
			success: function(res) {
				$("#nama").val(res.data.name);
				$("#email").val(res.data.email);
				$("#tentang").val(res.data.about);
			}
		});
		$('#modalDetail').modal('show'); // show bootstrap modal
	}

	function mdlhapus(id){
		$("#delIDPek").val(id);
		$('#mdlHapus').modal('show'); // show bootstrap modal
	}

	function hapus(){
		var id = $("#delIDPek").val();
		console.log(id);
		$.post("<?php echo site_url('core/user/hapus');?>", { 
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo $this->security->get_csrf_hash(); ?>',
			id: id
		}, function(res, status) {
			console.log(res);
			if(res.status){
				$("#histori"+id).remove();

				var textAlert;
				textAlert = "<div class=\"alert alert-success alert-dismissable\">";
				textAlert += "	<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				textAlert += "	<h4><i class=\"icon fa fa-check\"></i> Sukses!</h4>";
				textAlert += "	"+res.message;
				textAlert += "</div>";

				$("#alert").append(textAlert);
			}else{
				var textAlert;
				textAlert = "<div class=\"alert alert-warning alert-dismissable\">";
				textAlert += "	<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				textAlert += "	<h4><i class=\"icon fa fa-warning\"></i> Pesan!</h4>";
				textAlert += "	"+res.message;
				textAlert += "</div>";

				$("#alert").append(textAlert);
			}
			console.log(status);
		}, "json");
	}
</script>

<div id="mdlHapus" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<input class="form-control" id="delIDPek" type="hidden" value="0">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Konfirmasi</h3>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						Apakah benar akan di hapus?
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Batal</button>
				<button class="btn btn-sm btn-danger pull-right" onclick="hapus()" data-dismiss="modal">
					<i class="ace-icon fa fa-trash"></i>
					Hapus
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div id="modalDetail" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Lihat <?php echo $title;?></h3>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<strong>Nama</strong>
						<input type="text" id="nama" disabled readonly="true" class="form-control" value="" />
					</div>
					<div class="col-md-6">
						<strong>Email</strong>
						<input type="text" id="email" disabled readonly="true" class="form-control" value="" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<strong>Tentang</strong>
						<textarea id="tentang" disabled readonly="true" class="form-control"></textarea>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div id="modalFoto" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Lihat Foto</h3>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12" style="text-align:center;">
						<img src="<?php echo base_url('assets/upload/default.png'); ?>" id="fotona" width="220px" height="220px">
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>