<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php if(!empty($message)): ?>
			    	<div class="alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-warning"></i> Alert!</h4>
						<?php echo $message;?>
					</div>
					<?php endif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">

							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

									<div class="form-group">
										<label for="inputName" class="col-sm-2 control-label">Nama</label>
										<div class="col-sm-10">
											<input type="text" name="nama" class="form-control" value="<?php echo (!empty($datana['name'])) ? $datana['name'] : '';?>" id="inputName" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputAbout" class="col-sm-2 control-label">About</label>
										<div class="col-sm-10">
											<textarea name="about" class="form-control" id="inputAbout" placeholder="About"><?php echo (!empty($datana['about'])) ? $datana['about'] : '';?></textarea>
										</div>
									</div>

									<div class="form-group">
										<label for="inputEmail" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<input type="email" name="email" class="form-control" value="<?php echo (!empty($datana['email'])) ? $datana['email'] : '';?>" id="inputEmail" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputUsername" class="col-sm-2 control-label">Username</label>
										<div class="col-sm-10">
											<input type="text" name="username" class="form-control" value="<?php echo (!empty($datana['username'])) ? $datana['username'] : '';?>" id="inputUsername" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputPass" class="col-sm-2 control-label">Password</label>
										<div class="col-sm-10">
											<input type="password" name="pass" class="form-control" value="" id="inputPass" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputPassConf" class="col-sm-2 control-label">Retype Password</label>
										<div class="col-sm-10">
											<input type="password" name="passconf" class="form-control" value="" id="inputPassConf" placeholder="">
										</div>
									</div>

									<?php if(!empty($datana['username'])): ?>
									<div class="form-group">
										<div class="col-sm-12 text-center text-red">
											*Kosongkan password bila tidak di ubah.
										</div>
									</div>
									<?php endif; ?>

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Simpan">
											<a href="<?php echo site_url('core/user');?>" class="btn btn-primary" role="button">Batal</a>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->

				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->