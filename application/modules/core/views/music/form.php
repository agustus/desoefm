<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php if(!empty($message)): ?>
			    	<div class="alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-warning"></i> Alert!</h4>
						<?php echo $message;?>
					</div>
					<?php endif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">

							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

									<div class="form-group">
										<label for="inputTitle" class="col-sm-2 control-label">Judul</label>
										<div class="col-sm-10">
											<input type="text" name="title" class="form-control" value="<?php echo (!empty($datana['title'])) ? $datana['title'] : '';?>" id="inputTitle" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputArtist" class="col-sm-2 control-label">Artist</label>
										<div class="col-sm-10">
											<input type="text" name="artist" class="form-control" value="<?php echo (!empty($datana['artist'])) ? $datana['artist'] : '';?>" id="inputArtist" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputAlbum" class="col-sm-2 control-label">Album</label>
										<div class="col-sm-10">
											<input type="text" name="album" class="form-control" value="<?php echo (!empty($datana['album'])) ? $datana['album'] : '';?>" id="inputAlbum" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputDeskripsi" class="col-sm-2 control-label">Deskripsi</label>
										<div class="col-sm-10">
											<textarea name="deskripsi" class="form-control" id="inputDeskripsi" placeholder="About"><?php echo (!empty($datana['description'])) ? $datana['description'] : '';?></textarea>
										</div>
									</div>

									<div class="form-group">
										<label for="inputLink" class="col-sm-2 control-label">Link URL</label>
										<div class="col-sm-10">
											<input type="text" name="link" class="form-control" value="<?php echo (!empty($datana['link'])) ? $datana['link'] : '';?>" id="inputLink" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Simpan">
											<a href="<?php echo site_url('core/music');?>" class="btn btn-primary" role="button">Batal</a>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->

				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->