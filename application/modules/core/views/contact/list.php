<!-- Content Wrapper. Contains page content -->

	<div class="content-wrapper">

		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                if(!empty($breadcumb)):
            ?>
                <ol class="breadcrumb">
            <?php
                    foreach ($breadcumb as $breadcumb):
                        if(empty($breadcumb['link'])):
            ?>
                            <li class="active"><?php echo $breadcumb['judul'];?></li>
            <?php
                        else:
            ?>
                            <li>
                                <a href="<?php echo $breadcumb['link'];?>">
                                    <?php echo $breadcumb['judul'];?>
                                </a>
                            </li>
            <?php
                        endif;
                    endforeach;
            ?>
                </ol>
            <?php
                endif;
            ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div id="alert"></div>
					<?php if(!empty($message)): ?>
						<?php echo $message;?>
					<?php endif;?>
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">List <?php echo $title;?></h3>
						</div><!-- /.box-header -->
						<div class="box-body">
							<table class="table table-bordered">
								<tr>
									<th style="width: 20px">#</th>
									<th>Nama</th>
									<th>Email</th>
									<th>Tanggal Dibuat</th>
									<th style="width: 10%">Aksi</th>
								</tr>
								<?php
									if(!empty($data)):
										foreach ($data as $data):
								?>
								<tr id="histori<?php echo $data['id'];?>">
									<td>
										<?php echo $data['no'];?>
									</td>
									<td>
										<?php echo $data['nama'];?>
									</td>
									<td>
										<?php echo $data['email'];?>
									</td>
									<td>
										<?php echo $data['date_created'];?>
									</td>
									<td style="width: 20%">
										<button class="btn btn-xs btn-primary" onclick="view(<?php echo $data['id'];?>)" title="Lihat">
											<i class="fa fa-eye"></i>
										</button>
									</td>
								</tr>
								<?php
										endforeach;
									else:
								?>
								<tr>
									<td colspan="7">Tidak ada data.</td>
								</tr>
								<?php
									endif;
								?>
							</table>
						</div><!-- /.box-body -->
						<div class="box-footer clearfix">
							<?php echo (!empty($halaman)) ? $halaman:'';?>
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

<script type="text/javascript">
	function view(obj){
		var id = obj;

		$.ajax({
			url:"<?php echo site_url('core/contact/ambilSatu')?>/"+id,
			type:'get',
			dataType: 'json',
			success: function(res) {
				$("#nama").val(res.data.name);
				$("#email").val(res.data.email);
				$("#message").val(res.data.message);
			}
		});
		$('#modalDetail').modal('show'); // show bootstrap modal
	}
</script>

<div id="modalDetail" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Lihat <?php echo $title;?></h3>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<strong>Nama</strong>
						<input type="text" id="nama" disabled readonly="true" class="form-control" value="" />
					</div>
					<div class="col-md-6">
						<strong>Email</strong>
						<input type="text" id="email" disabled readonly="true" class="form-control" value="" />
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<strong>Pesan</strong>
						<textarea id="message" disabled readonly="true" class="form-control"></textarea>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>