<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php if(!empty($message)): ?>
			    	<div class="alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-warning"></i> Alert!</h4>
						<?php echo $message;?>
					</div>
					<?php endif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">

							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

									<div class="form-group">
										<label for="inputMusic" class="col-sm-2 control-label">Music</label>
										<div class="col-sm-5">
											<select name="music" class="form-control select2" id="inputMusic">
												<option value="">Music</option>
												<?php
													foreach ($music as $row) {
													
												?>
												<option value="<?php echo $row['id'];?>" <?php echo(!empty($datana['music'])) ? ($datana['music'] == $row['id']) ? "selected" : "" : "";?>><?php echo $row['title']." - ".$row['artist'];?></option>
												<?php
													}
												?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label for="inputPosition" class="col-sm-2 control-label">Posisi</label>
										<div class="col-sm-5">
											<input type="text" name="position" class="form-control" value="<?php echo (!empty($datana['position'])) ? $datana['position'] : '';?>" id="inputName" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputStatus" class="col-sm-2 control-label">Status</label>
										<div class="col-sm-2">
											<select name="status" class="form-control" id="inputStatus">
												<option value="">Status</option>
												<option value="1" <?php echo(!empty($datana['status'])) ? ($datana['status'] == 1) ? "selected" : "" : "";?>>Aktif</option>
												<option value="0" <?php echo(!empty($datana['status'])) ? ($datana['status'] == 0) ? "selected" : "" : "";?>>Tidak Aktif</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Simpan">
											<a href="<?php echo site_url('core/crew');?>" class="btn btn-primary" role="button">Batal</a>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->

				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

	<script type="text/javascript">
		$(function() {
			$('.select2').select2();
		})
	</script>