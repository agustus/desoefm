	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                if(!empty($breadcumb)):
            ?>
                <ol class="breadcrumb">
            <?php
                    foreach ($breadcumb as $breadcumb):
                        if(empty($breadcumb['link'])):
            ?>
                            <li class="active"><?php echo $breadcumb['judul'];?></li>
            <?php
                        else:
            ?>
                            <li>
                                <a href="<?php echo $breadcumb['link'];?>">
                                    <?php echo $breadcumb['judul'];?>
                                </a>
                            </li>
            <?php
                        endif;
                    endforeach;
            ?>
                </ol>
            <?php
                endif;
            ?>
		</section>

		<!-- Main content -->
		<section class="content">

			<div class="row">
				<div class="col-md-3">
					<!-- Profile Image -->
					<div class="box box-primary">
						<div class="box-body box-profile">
							<img class="profile-user-img img-responsive img-circle" src="<?php echo gravatar($akunInfo['email']);?>" alt="User profile picture">

							<h3 class="profile-username text-center"><?php echo $akunInfo['name'];?></h3>

							<p class="text-muted text-center"><?php echo $akunInfo['username'];?></p>
						</div>
					<!-- /.box-body -->
					</div>
					<!-- /.box -->

					<!-- About Me Box -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Info</h3>
						</div>

						<!-- /.box-header -->
						<div class="box-body">
							<strong><i class="fa fa-user margin-r-5"></i> Username</strong>

							<p class="text-muted">
								<?php echo $akunInfo['username'];?>
							</p>

							<hr>

							<strong><i class="fa fa-sticky-note margin-r-5"></i> About</strong>

							<p class="text-muted">
								<?php echo $akunInfo['about'];?>
							</p>

							<hr>

							<strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>

							<p class="text-muted"><?php echo $akunInfo['email'];?></p>

							<!-- <p>
								<span class="label label-danger">UI Design</span>
								<span class="label label-success">Coding</span>
								<span class="label label-info">Javascript</span>
								<span class="label label-warning">PHP</span>
								<span class="label label-primary">Node.js</span>
							</p>

							<hr> -->
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>

				<!-- /.col -->
				<div class="col-md-9">

					<?php echo $notif;?>

					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#info" data-toggle="tab">Change Info</a></li>
							<li><a href="#password" data-toggle="tab">Change Password</a></li>
						</ul>

						<div class="tab-content">
							<div class="active tab-pane" id="info">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

									<div class="form-group">
										<label for="nama" class="col-sm-2 control-label">Full Name</label>
										<div class="col-sm-10">
											<input type="text" name="nama" class="form-control" value="<?php echo (!empty($akunInfo['name'])) ? $akunInfo['name'] : set_value('nama');?>" id="nama" placeholder="">
										</div>
									</div>

									<!-- <div class="form-group">
										<label for="tempat_lahir" class="col-sm-2 control-label">Place of birth</label>
										<div class="col-sm-10">
											<input type="text" name="tempat_lahir" class="form-control" value="<?php echo (!empty($akunInfo['tempat_lahir'])) ? $akunInfo['tempat_lahir'] : '';?>" id="tempat_lahir" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="tgl_lahir" class="col-sm-2 control-label">Date of Birth</label>
										<div class="col-sm-10">
											<input type="text" name="tgl_lahir" class="form-control" value="<?php echo (!empty($akunInfo['tanggal_lahir'])) ? $akunInfo['tanggal_lahir'] : date("Y-m-d");?>" id="tgl_lahir" placeholder="">
										</div>
									</div> -->

									<!-- <div class="form-group">
										<label for="jk" class="col-sm-2 control-label">Gender</label>
										<div class="col-sm-10">
											<input type="radio" name="jk"<?php echo (strtolower(@$akunInfo['jenis_kelamin'])!='wanita')? ' checked':'';?> value="Pria"> Male
											<input type="radio" name="jk"<?php echo (strtolower(@$akunInfo['jenis_kelamin'])=='wanita')? ' checked':'';?> value="Wanita"> Female
										</div>
									</div>	 -->

									<div class="form-group">
										<label for="about" class="col-sm-2 control-label">About</label>
										<div class="col-sm-10">
											<textarea name="about" class="form-control" id="about" placeholder=""><?php echo (!empty($akunInfo['about'])) ? $akunInfo['about'] : set_value('about');?></textarea>
										</div>
									</div>			

									<!-- <div class="form-group">
										<label for="telp" class="col-sm-2 control-label">No. Telp</label>
										<div class="col-sm-10">
											<input type="text" name="telp" class="form-control" value="<?php echo (!empty($akunInfo['telp'])) ? $akunInfo['telp'] : set_value('telp');?>" id="telp" placeholder="">
										</div>
									</div> -->		

									<div class="form-group">
										<label for="email" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<input type="email" name="email" class="form-control" value="<?php echo (!empty($akunInfo['email'])) ? $akunInfo['email'] : set_value('email');?>" id="email" placeholder="">
										</div>
									</div>								

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="ubahInfo" value="Save">
										</div>
									</div>
								</form>
							</div>

							<div class="tab-pane" id="password">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									
									<div class="form-group">
										<label for="password" class="col-sm-2 control-label">Old Password</label>
										<div class="col-sm-10">
											<input type="password" name="password_lama" class="form-control" value="<?php set_value('password_lama'); ?>" id="password_lama" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="password" class="col-sm-2 control-label">New Password</label>
										<div class="col-sm-10">
											<input type="password" name="password_baru" class="form-control" value="<?php set_value('password_baru'); ?>" id="password_baru" placeholder="">
										</div>
									</div>	

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="ubahPassword" value="Save">
										</div>
									</div>
								</form>
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- /.nav-tabs-custom -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->