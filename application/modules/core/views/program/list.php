<!-- Content Wrapper. Contains page content -->

	<div class="content-wrapper">

		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                if(!empty($breadcumb)):
            ?>
                <ol class="breadcrumb">
            <?php
                    foreach ($breadcumb as $breadcumb):
                        if(empty($breadcumb['link'])):
            ?>
                            <li class="active"><?php echo $breadcumb['judul'];?></li>
            <?php
                        else:
            ?>
                            <li>
                                <a href="<?php echo $breadcumb['link'];?>">
                                    <?php echo $breadcumb['judul'];?>
                                </a>
                            </li>
            <?php
                        endif;
                    endforeach;
            ?>
                </ol>
            <?php
                endif;
            ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div id="alert"></div>
					<?php if(!empty($message)): ?>
						<?php echo $message;?>
					<?php endif;?><!-- /.box-header -->

					<?php $i=0; // counter ?>

					<?php foreach ($allDay as $day): ?>

					<?php if ($i%4==0) { // if counter is multiple of 3 ?>
					<div class="row">
						<div class="box-body">
					<?php } ?>

						

							<div class="col-md-3">
								<div class="box box-success box-solid">
									<div class="box-header with-border">

										<h3 class="box-title"><?php echo $day['day'];?></h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
											</button>
										</div><!-- /.box-tools -->
									</div><!-- /.box-header -->
									<div class="box-body">
										<table class="table table-condensed">
											<tr>
												<td colspan="2">
													<a class="btn btn-primary btn-xs" href="<?php echo site_url('core/program/add/'.$day['id']);?>" title="Ubah">
														<i class="fa fa-plus"></i> Add Program
													</a>
												</td>
											</tr>
											<tr>
												<th>Time</th>
												<th>Program Name</th>
												<th style="width: 20px; text-align: center;">#</th>
											</tr>
											<?php
												if(!empty($day['program'])):
													foreach ($day['program'] as $data):
											?>
											<tr id="histori<?php echo $data['id'];?>">
												<td>
													<?php echo substr($data['time'], 0, 5);?>
												</td>
												<td>
													<?php echo $data['event'];?>
												</td>
												<td>
													<button class="btn btn-danger btn-xs" onclick="mdlhapus(<?php echo $data['id'];?>)" title="Delete Program">
														<i class="glyphicon glyphicon-trash"></i>
													</button>
												</td>
											</tr>
											<?php
													endforeach;
												else:
											?>
											<tr>
												<td colspan="3">No Schedule.</td>
											</tr>
											<?php
												endif;
											?>
										</table>
									</div><!-- /.box-body -->
								</div><!-- /.box -->
							</div><!-- /.col -->
							
						

						<?php $i++; ?>

					<?php if($i%4==0) { // if counter is multiple of 3 ?>
						</div>
					</div>
					<?php } ?>

					<?php endforeach; ?>

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

<script type="text/javascript">
	function mdlhapus(id){
		$("#delIDPek").val(id);
		$('#mdlHapus').modal('show'); // show bootstrap modal
	}

	function hapus(){
		var id = $("#delIDPek").val();
		console.log(id);
		$.post("<?php echo site_url('core/program/hapus');?>", { 
			<?php echo $this->security->get_csrf_token_name(); ?> : '<?php echo $this->security->get_csrf_hash(); ?>',
			id: id
		}, function(res, status) {
			console.log(res);
			if(res.status){
				$("#histori"+id).remove();

				var textAlert;
				textAlert = "<div class=\"alert alert-success alert-dismissable\">";
				textAlert += "	<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				textAlert += "	<h4><i class=\"icon fa fa-check\"></i> Sukses!</h4>";
				textAlert += "	"+res.message;
				textAlert += "</div>";

				$("#alert").append(textAlert);
			}else{
				var textAlert;
				textAlert = "<div class=\"alert alert-warning alert-dismissable\">";
				textAlert += "	<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				textAlert += "	<h4><i class=\"icon fa fa-warning\"></i> Pesan!</h4>";
				textAlert += "	"+res.message;
				textAlert += "</div>";

				$("#alert").append(textAlert);
			}
			console.log(status);
		}, "json");
	}
</script>

<div id="mdlHapus" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<input class="form-control" id="delIDPek" type="hidden" value="0">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Konfirmasi</h3>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						Apakah benar akan di hapus?
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Batal</button>
				<button class="btn btn-sm btn-danger pull-right" onclick="hapus()" data-dismiss="modal">
					<i class="ace-icon fa fa-trash"></i>
					Hapus
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>