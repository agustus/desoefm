<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php if(!empty($message)): ?>
			    	<div class="alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-warning"></i> Alert!</h4>
						<?php echo $message;?>
					</div>
					<?php endif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">

							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

									<div class="form-group">
										<label for="inputJam" class="col-sm-2 control-label">Time</label>
										<div class="col-sm-2 bootstrap-timepicker input-group">
											<input type="text" name="jam" class="form-control" value="<?php echo (!empty($datana['time'])) ? $datana['time'] : '';?>" id="inputJam" placeholder="">
											<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
										</div>
									</div>

									<div class="form-group">
										<label for="inputAcara" class="col-sm-2 control-label">Program Name</label>
										<div class="col-sm-5 input-group">
											<textarea name="acara" class="form-control" id="inputAcara" placeholder="Program Name"><?php echo (!empty($datana['event'])) ? $datana['event'] : '';?></textarea>
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Save">
											<a href="<?php echo site_url('core/program');?>" class="btn btn-primary" role="button">Cancel</a>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->

				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

<script>
  $(function () {
    //Timepicker
    $("#inputJam").timepicker({
      showInputs: false,
      maxHours: 24,
      showMeridian: false,
      defaultTime: '00:00'
    });
  });
</script>