<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php if(!empty($message)): ?>
			    	<div class="alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-warning"></i> Alert!</h4>
						<?php echo $message;?>
					</div>
					<?php endif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">

							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

									<div class="form-group">
										<label for="inputDeskripsi" class="col-sm-2 control-label">Konten</label>
										<div class="col-sm-10">
											<textarea name="content" class="form-control" id="inputDeskripsi" placeholder="Konten"><?php echo (!empty($datana['content'])) ? $datana['content'] : '';?></textarea>
										</div>
									</div>

									<div class="form-group">
										<label for="inputArtist" class="col-sm-2 control-label">Bulan</label>
										<div class="col-sm-10">
											<select name="bulan" class="form-control">
												<?php for($i=1;$i<=12;$i++):?>
												<?php $bln = ($i<10)?'0'.$i:$i;?>
												<option<?php echo (!empty($datana['bulan'])) ? ($bln==$datana['bulan'])?' selected':'' :'';?> value="<?php echo ($i<10)?'0':'';?><?php echo $i;?>">
													<?php echo ($i<10)?'0':'';?><?php echo $i;?>
												</option>
												<?php endfor;?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label for="inputAlbum" class="col-sm-2 control-label">Tahun</label>
										<div class="col-sm-10">
											<select name="tahun" class="form-control">
												<?php for($i=date("Y");$i<=date("Y")+5;$i++):?>
												<option<?php echo (!empty($datana['tahun']))? ($i==$datana['tahun'])?' selected':'' : '';?> value="<?php echo $i;?>">
													<?php echo $i;?>
												</option>
												<?php endfor;?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Simpan">
											<a href="<?php echo site_url('core/theme');?>" class="btn btn-primary" role="button">Batal</a>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->

				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->