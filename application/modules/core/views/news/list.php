<style type="text/css">
	img{
		height: 100%;
		width: 100%;
	}

	#content_artikel{
		word-wrap: break-word;
	}
</style>
<script>
            tinymce.init({
                selector: "textarea",
                theme: "modern",
                // width: 850,
                height: 300,
                relative_urls : false,
                remove_script_host: false,
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                     "save table contextmenu directionality emoticons template paste textcolor"
               ],
               // content_css: "css/content.css",
               toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
               style_formats: [
                    {title: "Bold text", inline: "b"},
                    {title: "Red text", inline: "span", styles: {color: "#ff0000"}},
                    {title: "Red header", block: "h1", styles: {color: "#ff0000"}},
                    {title: "Example 1", inline: "span", classes: "example1"},
                    {title: "Example 2", inline: "span", classes: "example2"},
                    {title: "Table styles"},
                    {title: "Table row 1", selector: "tr", classes: "tablerow1"}
                ],
                external_filemanager_path:"<?php echo base_url('assets/filemanager');?>/",
                filemanager_title:"Filemanager" ,
                filemanager_access_key:"dikaGanteng",
                external_plugins: { "filemanager" : "<?php echo base_url('assets/filemanager/plugin.min.js');?>" }
             }); 
            </script>

<!--Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- AWARD -->
		<section class="content">
			<div class="row">
				<div class=""><!-- col-md-12 -->
					<?php echo $notif;?>
					
					<div class="tab-content">

						<div class="<?php echo (!empty($activeList)) ?"active ": '';?>tab-pane" id="listData">
							<section class="content">
								<div class="row">
									<div class="col-xs-12">
											<a class="btn btn-primary btn-md" href="<?php echo $href_add;?>">
												<i class="fa fa-plus"></i> <?php echo $btn_add;?>
											</a> <br></br>
										<!-- <?php echo $notif;?> -->
										<div id="alert"></div>
										<div class="box">
											<div class="box-header with-border">
												<h3 class="box-title">List <?php echo $title;?></h3>
											</div><!-- /.box-header -->
											<div class="box-body">
												<table class="table table-bordered">
												<tr>
													<th style="width: 20px">#</th>
													<th>Title</th>
													<th>Status</th>
													<th>Date Created</th>
													<th style="width: 10%">Action</th>
												</tr>
													<?php
												if(!empty($hasilData)):
													foreach ($hasilData as $hasilData):
											?>
											<tr id="hijidata<?php echo $hasilData['id'];?>">
												<td>
													<?php echo $hasilData['no'];?>
												</td>
												<td>
													<?php echo $hasilData['title'];?>
												</td>
												<td>
													<?php echo $hasilData['status'];?>
												</td>
												<td>
													<?php echo $hasilData['date_created'];?>
												</td>
												<td style="width: 20%">
													<button class="btn btn-xs btn-primary" title="View" onclick="lihat(<?php echo $hasilData['id'];?>)">
														<i class="fa fa-eye"></i>
													</button>
													<a class="btn btn-default btn-xs" href="<?php echo $hasilData['href_edit'];?>" title="Ubah">
														<i class="fa fa-pencil"></i>
													</a>
													<button class="btn btn-danger btn-xs" title="Hapus" onclick="mdlhapus(<?php echo $hasilData['id'];?>)">
														<i class="glyphicon glyphicon-trash"></i>
													</button>
												</td>
											</tr>
											<?php
													endforeach;
												else:
											?>
											<tr>
												<td colspan="7">Tidak ada data.</td>
											</tr>
											<?php
												endif;
											?>
										</table>
											</div><!-- /.box-body -->
											<div class="box-footer clearfix">
												<?php echo (!empty($halaman)) ? $halaman:'';?>
											</div>
										</div>
									</div><!-- /.col -->
								</div><!-- /.row -->
							</section><!-- /.content -->
						</div>
						<!-- End Of List -->

					</div><!-- /.tab-content -->
					
				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
		<!-- BATAS MAIN AWARD -->

	</div><!-- /.content-wrapper -->


	<div id="modalBanner" class="modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<input class="form-control" id="delIDPek" type="hidden" value="0">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="smaller lighter blue no-margin">Choose Image</h3>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<iframe width="560" height="400" src="<?php echo base_url('assets');?>/filemanager/dialog.php?type=1&amp;field_id=banner&amp;relative_url=1&amp;akey=dikaGanteng" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div id="modalLogo" class="modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<input class="form-control" id="delIDPek" type="hidden" value="0">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="smaller lighter blue no-margin">Choose Image</h3>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<iframe width="560" height="400" src="<?php echo base_url('assets');?>/filemanager/dialog.php?type=1&amp;field_id=logo&amp;relative_url=1&amp;akey=dikaGanteng" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


<script type="text/javascript">
	function lihat(id){
		$.ajax({
			url:"<?php echo $href_get_one; ?>/"+id,
			type:'get',
			dataType: 'json',
			success: function(data) {
				console.log(data);

				$("#title_artikel").html("<h3 class=\"smaller lighter blue no-margin\">"+data.title+"</h3>");
				$("#content_artikel").html(data.content);
				$("#date_artikel").html("Date : "+data.date_created.substr(0, 10));
			}
		});

		$('#modalUser').modal('show'); // show bootstrap modal
	}

	function mdlhapus(id){
		$("#delIDPek").val(id);
		$('#mdlHapus').modal('show'); // show bootstrap modal
	}

	function hapus(){
		var id = $("#delIDPek").val();

		$.post("<?php echo $href_hapus;?>", { 
			<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>',
			id: id
		}, function(res) {
			if(res.status){
				$("#hijidata"+id).remove();

				var textAlert;
				textAlert = "<div class=\"alert alert-success alert-dismissable\">";
				textAlert += "	<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				textAlert += "	<h4><i class=\"icon fa fa-check\"></i> Success!</h4>";
				textAlert += "	Pesan : "+res.message;
				textAlert += "</div>";

				$("#alert").append(textAlert);
			}else{
				var textAlert;
				textAlert = "<div class=\"alert alert-warning alert-dismissable\">";
				textAlert += "	<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				textAlert += "	<h4><i class=\"icon fa fa-warning\"></i> Perhatian!</h4>";
				textAlert += "	Pesan : "+res.message;
				textAlert += "</div>";

				$("#alert").append(textAlert);
			}
		}, "json");
	}
</script>

<div id="mdlHapus" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<input class="form-control" id="delIDPek" type="hidden" value="0">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Konfirmasi</h3>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						Apakah benar akan di hapus?
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Cancel</button>
				<button class="btn btn-sm btn-danger pull-right" onclick="hapus()" data-dismiss="modal">
					<i class="ace-icon fa fa-trash"></i>
					Delete
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div id="modalUser" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin" id="title_artikel">Data Artikel</h3>
				<p id="date_artikel"></p>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<p id="content_artikel"></p>
						</div>
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
</div>


	<script>
		function responsive_filemanager_callback(field_id){
			console.log(field_id);
			var url=jQuery('#'+field_id).val();
			// alert('update '+field_id+" with "+url);
			// jQuery('#url_foto').val('<?php echo base_url('assets/upload');?>/'+url);
			//your code
			jQuery('#'+field_id+field_id).val(url);
			jQuery('#'+field_id).attr('src','<?php echo base_url('assets/upload');?>/'+url);
		}
	</script>