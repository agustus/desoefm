
<script>
            tinymce.init({
                selector: "textarea",
                theme: "modern",
                // width: 850,
                height: 300,
                relative_urls : false,
                remove_script_host: false,
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                     "save table contextmenu directionality emoticons template paste textcolor"
               ],
               // content_css: "css/content.css",
               toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
               style_formats: [
                    {title: "Header 3", block: "h3"},
                    {title: "Header 2", block: "h2"},
                    {title: "Header 1", block: "h1"},
                    // {title: "Bold text", inline: "b"},
                    // {title: "Red text", inline: "span", styles: {color: "#ff0000"}},
                    // {title: "Red header", block: "h1", styles: {color: "#ff0000"}},
                    // {title: "Example 1", inline: "span", classes: "example1"},
                    // {title: "Table row 1", selector: "tr", classes: "tablerow1"}
                ],
                external_filemanager_path:"<?php echo base_url('assets/filemanager');?>/",
                filemanager_title:"Filemanager" ,
                filemanager_access_key:"dikaGanteng",
                external_plugins: { "filemanager" : "<?php echo base_url('assets/filemanager/plugin.min.js');?>" }
             }); 
            </script>

<!--Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php echo $notif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">
                  
							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									
									<div class="form-group">
										<label for="title" class="col-sm-2 control-label">Title</label>
										<div class="col-sm-10">
											<input type="text" name="title" class="form-control" value="<?php echo (!empty($datana['title'])) ? $datana['title'] : set_value('title');?>" id="title" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="content" class="col-sm-2 control-label">Content</label>
										<div class="col-sm-10">
											<textarea name="content" id="content" placeholder=""><?php echo (!empty($datana['content'])) ? $datana['content'] : set_value('content');?></textarea>
										</div>
									</div>			

									<div class="form-group">
										<label for="tag" class="col-sm-2 control-label">Status</label>
										<div class="col-sm-10">
											<select name="status" class="form-control">
												<option value="0"<?php echo (!empty($datana['status'])) ? ($datana['status']==0) ? ' selected' : '' : '';?>>Draft</option>
												<option value="1"<?php echo (!empty($datana['status'])) ? ($datana['status']==1) ? ' selected' : '' : '';?>>Publish</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Save">
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->
					
				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->