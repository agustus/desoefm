<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">

		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel (optional) -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="<?php echo gravatar($akunInfo['email']);?>" class="img-circle" alt="User Image">
				</div>

				<div class="pull-left info">
					<p><?php echo $akunInfo['name'];?></p>
					<!-- Status -->
					<a href="<?php echo site_url('profile');?>"><?php echo $akunInfo['username'];?></a>
				</div>
			</div>

			<!-- search form (Optional)
			<form action="#" method="get" class="sidebar-form">
				<div class="input-group">
					<input type="text" name="q" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</form> -->

			<!-- Sidebar Menu -->
			<ul class="sidebar-menu">
				<li class="header">Main Navigation</li>
				<!-- Optionally, you can add icons to the links -->
				<li<?php echo($active_menu=='dashboard')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/dashboard');?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
				</li>

				<li class="treeview<?php echo($active_menu=='about' || $active_menu=='history')? " active": "";?>">
					<a href="#"><i class="fa fa-briefcase"></i> <span>Company</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li<?php echo($active_menu=='about')? " class=\"active\"": "";?>>
							<a href="<?php echo site_url('core/page/about');?>"><i class="fa fa-circle-o text-red"></i> <span>About</span></a>
						</li>

						<li<?php echo($active_menu=='history')? " class=\"active\"": "";?>>
							<a href="<?php echo site_url('core/page/history');?>"><i class="fa fa-circle-o text-red"></i> <span>History</span></a>
						</li>
					</ul>
				</li>

				<li class="treeview<?php echo($active_menu=='program' || $active_menu=='planning')? " active": "";?>">
					<a href="#"><i class="fa fa-podcast"></i> <span>Program</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li<?php echo($active_menu=='program')? " class=\"active\"": "";?>>
							<a href="<?php echo site_url('core/program');?>"><i class="fa fa-circle-o text-red"></i> <span>Broadcast Schedule</span></a>
						</li>

						<li<?php echo($active_menu=='planning')? " class=\"active\"": "";?>>
							<a href="<?php echo site_url('core/planning');?>"><i class="fa fa-circle-o text-red"></i> <span>Planning</span></a>
						</li>
					</ul>
				</li>

				<li<?php echo($active_menu=='news')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/news');?>"><i class="fa fa-newspaper-o"></i> <span>News</span></a>
				</li>

				<li<?php echo($active_menu=='blog')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/blog');?>"><i class="fa fa-newspaper-o"></i> <span>Blog</span></a>
				</li>				

				<li class="treeview<?php echo($active_menu=='galery_event' || $active_menu=='galery_social')? " active": "";?>">
					<a href="#"><i class="fa fa-photo"></i> <span>Gallery</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li<?php echo($active_menu=='galery_event')? " class=\"active\"": "";?>>
							<a href="<?php echo site_url('core/gallery/event');?>"><i class="fa fa-circle-o text-red"></i> Event</a>
						</li>
						<li<?php echo($active_menu=='galery_social')? " class=\"active\"": "";?>>
							<a href="<?php echo site_url('core/gallery/social');?>"><i class="fa fa-circle-o text-red"></i> Social</a>
						</li>
					</ul>
				</li>

				<!-- <li<?php echo($active_menu=='theme')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/theme');?>"><i class="fa fa-star"></i> <span>Month Theme</span></a>
				</li> -->

				<!-- <li<?php echo($active_menu=='banner')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/banner');?>"><i class="fa fa-picture-o"></i> <span>Banner</span></a>
				</li> -->

				<li<?php echo($active_menu=='crew')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/crew');?>"><i class="fa fa-users"></i> <span>Crew</span></a>
				</li>

				<!-- <li<?php echo($active_menu=='music')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/music');?>"><i class="fa fa-headphones"></i> <span>Music</span></a>
				</li> -->

				<!-- <li <?php echo($active_menu=='iklan')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/iklan');?>"><i class="fa fa-bullhorn"></i> <span>Iklan</span></a>
				</li> -->

				<li <?php echo($active_menu=='contact')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/contact');?>"><i class="fa fa-comments"></i> <span>Contact</span></a>
				</li>

				<li<?php echo($active_menu=='user')? " class=\"active\"": "";?>>
					<a href="<?php echo site_url('core/user');?>"><i class="fa fa-user"></i> <span>Users</span></a>
				</li>

				<li>
					<a href="<?php echo site_url('core/dashboard/logout');?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a>
				</li>
			</ul><!-- /.sidebar-menu -->
		</section>
		<!-- /.sidebar -->
	</aside>
