<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php if(!empty($message)): ?>
			    	<div class="alert alert-warning alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-warning"></i> Alert!</h4>
						<?php echo $message;?>
					</div>
					<?php endif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">

							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

									<div class="form-group">
										<label for="banner" class="col-sm-2 control-label">Photo</label>
										<div class="col-sm-10">
											<input type="hidden" id="bannerbanner" name="photo" value="<?php echo (!empty($datana['photo'])) ? $datana['photo'] : '';?>">

											<a data-toggle="modal" href="javascript:;" data-target="#modalPhoto" type="button" id="thumb-image0" class="img-thumbnail">
												<img src="<?php echo (!empty($datana['photo'])) ? base_url("assets/upload/".$datana['photo']) : base_url("assets/upload/crew/default.png"); ?>" id="banner" height="200px" width="200px" alt="1" title="" />
											</a>
										</div>
									</div>

									<div class="form-group">
										<label for="inputName" class="col-sm-2 control-label">Name</label>
										<div class="col-sm-10">
											<input type="text" name="name" placeholder="Name" class="form-control" value="<?php echo (!empty($datana['name'])) ? $datana['name'] : '';?>" id="inputName" placeholder="">
										</div>
									</div>

									<div class="form-group">
										<label for="inputDeskripsi" class="col-sm-2 control-label">Description</label>
										<div class="col-sm-10">
											<textarea name="deskripsi" class="form-control" id="inputDeskripsi" placeholder="About"><?php echo (!empty($datana['description'])) ? $datana['description'] : '';?></textarea>
										</div>
									</div>

									<div class="form-group">
										<label for="inputJabatan" class="col-sm-2 control-label"> Jabatan </label>
										<div class="col-sm-2">
											<select name="jabatan" class="form-control" id="inputJabatan">
												<option value="">Jabatan</option>
												<?php
													foreach ($data_jabatan as $row) {
													
												?>
												<option value="<?php echo $row['id'];?>" <?php echo(!empty($datana['jabatan'])) ? ($datana['jabatan'] == $row['id']) ? "selected" : "" : "";?>><?php echo $row['name'];?></option>
												<?php
													}
												?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label for="iFb" class="col-sm-2 control-label">Facebook</label>
										<div class="col-sm-10">
							                <input type="text" name="facebook" class="form-control" placeholder="http://facebook.com" value="<?php echo (!empty($datana['facebook'])) ? $datana['facebook'] : '';?>">
										</div>
									</div>

									<div class="form-group">
										<label for="iTweet" class="col-sm-2 control-label">Twitter</label>
										<div class="col-sm-10">
							                <input type="text" name="twitter" class="form-control" placeholder="http://twitter.com" value="<?php echo (!empty($datana['twitter'])) ? $datana['twitter'] : '';?>">
										</div>
									</div>

									<div class="form-group">
										<label for="iLinkedin" class="col-sm-2 control-label">Linkedin</label>
										<div class="col-sm-10">
							                <input type="text" name="linkedin" class="form-control" placeholder="http://linkedin.com" value="<?php echo (!empty($datana['linkedin'])) ? $datana['linkedin'] : '';?>">
										</div>
									</div>

									<div class="form-group">
										<label for="iGplus" class="col-sm-2 control-label">Google Plus</label>
										<div class="col-sm-10">
							                <input type="text" name="gplus" class="form-control" placeholder="http://plus.google.com" value="<?php echo (!empty($datana['gplus'])) ? $datana['gplus'] : '';?>">
										</div>
									</div>

									<div class="form-group">
										<label for="inputStatus" class="col-sm-2 control-label">Status</label>
										<div class="col-sm-2">
											<select name="status" class="form-control" id="inputStatus">
												<option value="1" <?php echo(!empty($datana['status'])) ? ($datana['status'] == 1) ? "selected" : "" : "";?>>Aktif</option>
												<option value="0" <?php echo(!empty($datana['status'])) ? ($datana['status'] == 0) ? "selected" : "" : "";?>>Tidak Aktif</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Simpan">
											<a href="<?php echo site_url('core/crew');?>" class="btn btn-primary" role="button">Batal</a>
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->

				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

	<div id="modalPhoto" class="modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<input class="form-control" id="delIDPek" type="hidden" value="0">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="smaller lighter blue no-margin">Choose Image</h3>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<iframe width="560" height="400" src="<?php echo base_url('assets');?>/filemanager/dialog.php?type=1&amp;field_id=banner&amp;relative_url=1&amp;akey=dikaGanteng" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	
	<script>
		function responsive_filemanager_callback(field_id){
			console.log(field_id);
			var url=jQuery('#'+field_id).val();
			// alert('update '+field_id+" with "+url);
			// jQuery('#url_foto').val('<?php echo base_url('assets/upload');?>/'+url);
			//your code
			jQuery('#'+field_id+field_id).val(url);
			jQuery('#'+field_id).attr('src','<?php echo base_url('assets/upload');?>/'+url);
		}
	</script>