
<script>
            tinymce.init({
                selector: "textarea",
                theme: "modern",
                // width: 850,
                height: 300,
                relative_urls : false,
                remove_script_host: false,
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                     "save table contextmenu directionality emoticons template paste textcolor"
               ],
               // content_css: "css/content.css",
               toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
               style_formats: [
                    {title: "Header 3", block: "h3"},
                    {title: "Header 2", block: "h2"},
                    {title: "Header 1", block: "h1"},
                    // {title: "Bold text", inline: "b"},
                    // {title: "Red text", inline: "span", styles: {color: "#ff0000"}},
                    // {title: "Red header", block: "h1", styles: {color: "#ff0000"}},
                    // {title: "Example 1", inline: "span", classes: "example1"},
                    // {title: "Table row 1", selector: "tr", classes: "tablerow1"}
                ],
                external_filemanager_path:"<?php echo base_url('assets/filemanager');?>/",
                filemanager_title:"Filemanager" ,
                filemanager_access_key:"dikaGanteng",
                external_plugins: { "filemanager" : "<?php echo base_url('assets/filemanager/plugin.min.js');?>" }
             }); 
            </script>

<!--Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php echo $title;?>
				<small><?php echo $description;?></small>
			</h1>
			<?php
                            if(!empty($breadcumb)):
                        ?>
                            <ol class="breadcrumb">
                        <?php
                                foreach ($breadcumb as $breadcumb):
                                    if(empty($breadcumb['link'])):
                        ?>
                                        <li class="active"><?php echo $breadcumb['judul'];?></li>
                        <?php
                                    else:
                        ?>
                                        <li>
                                            <a href="<?php echo $breadcumb['link'];?>">
                                                <?php echo $breadcumb['judul'];?>
                                            </a>
                                        </li>
                        <?php
                                    endif;
                                endforeach;
                        ?>
                            </ol>
                        <?php
                            endif;
                        ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<?php echo $notif;?>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profil" data-toggle="tab">Info</a></li>
						</ul>

						<div class="tab-content">
                  
							<div class="active tab-pane" id="profil">
								<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									
									<!-- <div class="form-group">
										<label for="title" class="col-sm-2 control-label">Title</label>
										<div class="col-sm-10">
											
										</div>
									</div> -->
									<input type="hidden" name="title" class="form-control" value="<?php echo (!empty($datana['nama_page'])) ? $datana['nama_page'] : set_value('title');?>" id="title" placeholder="">

									<div class="form-group">
										<label for="banner" class="col-sm-2 control-label">Banner</label>
										<div class="col-sm-10">
											<input type="hidden" id="bannerbanner" name="banner" value="<?php echo (!empty($datana['banner'])) ? $datana['banner'] : '';?>">

											<a data-toggle="modal" href="javascript:;" data-target="#modalBanner" type="button" id="thumb-image0" class="img-thumbnail">
												<img src="<?php echo (!empty($datana['banner'])) ? base_url("assets/upload/".$datana['banner']) : base_url("assets/upload/default_banner.jpg"); ?>" id="banner" height="300" width="100%" alt="1" title="" />
											</a>
										</div>
									</div>

									<div class="form-group">
										<label for="content" class="col-sm-2 control-label">Content</label>
										<div class="col-sm-10">
											<textarea name="content" id="content" placeholder=""><?php echo (!empty($datana['content_page'])) ? $datana['content_page'] : set_value('content');?></textarea>
										</div>
									</div>			
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-success btn-md" name="simpan" value="Save">
										</div>
									</div>
								</form>
							</div><!-- /.tab-pane -->

						</div><!-- /.tab-content -->
					</div><!-- /.nav-tabs-custom -->
					
				</div><!-- /.col -->
			</div>
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

	<div id="modalBanner" class="modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<input class="form-control" id="delIDPek" type="hidden" value="0">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="smaller lighter blue no-margin">Choose Image</h3>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<iframe width="560" height="400" src="<?php echo base_url('assets');?>/filemanager/dialog.php?type=1&amp;field_id=banner&amp;relative_url=1&amp;akey=dikaGanteng" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Cancel</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	
	<script>
		function responsive_filemanager_callback(field_id){
			console.log(field_id);
			var url=jQuery('#'+field_id).val();
			// alert('update '+field_id+" with "+url);
			// jQuery('#url_foto').val('<?php echo base_url('assets/upload');?>/'+url);
			//your code
			jQuery('#'+field_id+field_id).val(url);
			jQuery('#'+field_id).attr('src','<?php echo base_url('assets/upload');?>/'+url);
		}
	</script>