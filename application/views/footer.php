</div> <!-- /.container-->
   </section>
   <!--End Contact-->
   
   
		<!--Start Footer-->
		<footer>
			<div class="container">
				<div class="row">
					<!--Start copyright-->
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="copyright">
							<p>
								<?php echo $_SESSION['site_footer'];?>
							</p>
						</div>
					</div>
					<!--End copyright-->

					<!--start social icons-->
					<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="social-icons">
					<ul>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li> <a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>
					</div>
					</div>
					<!--End social icons-->
				</div> <!-- /.row-->
			</div> <!-- /.container-->
		</footer>
		<!--End Footer-->

		<a href="#" class="scrollup"> <i class="fa fa-chevron-up"> </i> </a>

		<!--Plugins-->
		<script type="text/javascript" src="<?php echo $asset;?>js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/bootstrap.min.js"></script> 
		<script type="text/javascript" src="<?php echo $asset;?>js/owl-carousel/owl.carousel.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/jquery.flexslider-min.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/easing.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/jquery.easypiechart.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/jquery.appear.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/jquery.parallax-1.1.3.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/jquery.mixitup.min.js"></script>
		<script type="text/javascript" src="<?php echo $asset;?>js/custom.js"></script>
    
	</body>
</html>