				</div>
			</div>
			<!-- End Header -->

			<!-- News -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<!-- news -->
						<div class="col-md-8">
							<article class="box post">
								<!-- <a href="#" class="image featured"><img src="<?php echo $asset;?>img/pic01.jpg" alt="" /></a> -->
								<header>
									<h2><?php echo ucwords($datana['title']);?></h2>
								</header>
								<section>
									<p>
										<?php echo htmlspecialchars_decode($datana['content']);?>
									</p>
								</section>
							</article>
						</div>
						<!-- end of news -->

						<!-- Sidebar / iklan -->
						<div class="col-md-4">
							<section class="box">
							<?php
							if(!empty($data_iklan)):
								foreach ($data_iklan as $data):
							?>
								<a href="<?php echo $data['url'];?>" class="image">
									<img src="<?php echo $asset;?>upload/<?php echo $data['photo'];?>" alt="" />
								</a>
							<?php
								endforeach;
							endif;
							?>
							</section>
						</div>
						<!-- end of sidebar -->
					</div>	
				</div>
			</div>