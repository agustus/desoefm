				</div>
			</div>
			<!-- End Header -->

			<!-- News -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<!-- news -->
						<div class="col-md-8">
							<article class="box post">
								
								<header>
									<h2>News</h2>
									<p>News Eltras Radio Politeknik Negeri Bandung</p>
								</header>

								<?php if(!empty($news)): ?>
									<?php foreach($news as $resNews):?>
								<div class="post row">
									<div class="col-md-4">
										<img src="<?php echo $resNews['article_banner'];?>" width="100%" height="198px" />
									</div>
									<div class="col-md-8">
										<a href="<?php echo $resNews['link_detail'];?>" style="text-decoration:none;">
											<h3 class="title-news"><?php echo $resNews['article_title'];?></h3>
										</a>
										<h6>Oleh : <?php echo $resNews['nama_user'];?> | <?php echo $resNews['date_created'];?></h6>
										<p><?php echo $resNews['article_cuplikan'];?></p>
										<a href="<?php echo $resNews['link_detail'];?>" class="btn btn-sm btn-default" style="text-decoration:none; background-color:#FB0300;color:#ffffff;">
											Read More
										</a>
									</div>
					            </div>
					            	<?php endforeach;?>
						        <?php endif;?>

						        <?php echo $halaman;?>

					            <!-- <div class="box-footer clearfix">
					            	<ul class="pagination pagination-sm no-margin pull-right">
					            		<li class="disabled">
					            			<a href="#">1</a>
					            		</li>
					            		<li>
					            			<a href="#">2</a>
					            		</li>
					            		<li>
					            			<a href="#" rel="next">Lanjut &raquo;</a>
					            		</li>
					            	</ul>
					            </div> -->
							</article>
						</div>
						<!-- end of news -->

						<!-- Sidebar / iklan -->
						<div class="col-md-4">
							<section class="box">
							<?php
							if(!empty($data_iklan)):
								foreach ($data_iklan as $data):
							?>
								<a href="<?php echo $data['url'];?>" class="image">
									<img src="<?php echo $asset;?>upload/<?php echo $data['photo'];?>" alt="" />
								</a>
							<?php
								endforeach;
							endif;
							?>
							</section>
						</div>
						<!-- end of sidebar -->
					</div>	
				</div>
			</div>