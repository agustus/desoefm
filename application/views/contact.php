				</div>
			</div>
			<!-- End Header -->

			<!-- News -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<!-- news -->
						<div class="col-md-8">
							<article class="box post">
								<!-- <a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a> -->
								<header>
									<h2>Contact Us</h2>
									<p>Gedung Achmad Bakrie (Labtek VIII) Lantai 4, Institut Teknologi Bandung, Jln. Ganesa 10 Bandung</p>
									<p>LINE OFFICIAL: @GVV6637G | ASK.FM: eltrasradio | TWITTER: eltrasradio</p>
								</header>
								<section>
									<?php if(!empty($message)): ?>
							    	<div class="alert alert-warning alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<h4><i class="icon fa fa-warning"></i> Alert!</h4>
										<?php echo $message;?>
									</div>
									<?php endif;?>
									<div class="row">
										<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
										<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
										<div class="form-group">
											<label for="inputName" class="col-sm-2 control-label">Nama</label>
											<div class="col-sm-10">
												<input type="text" name="nama" class="form-control" value="" id="inputName" placeholder="Nama Lengkap">
											</div>
										</div>
										<div class="form-group">
											<label for="inputEmail" class="col-sm-2 control-label">Email</label>
											<div class="col-sm-10">
												<input type="text" name="email" class="form-control" value="" id="inputEmail" placeholder="Email">
											</div>
										</div>
										<div class="form-group">
											<label for="inputSub" class="col-sm-2 control-label">Subjek</label>
											<div class="col-sm-10">
												<input type="text" name="subjek" class="form-control" value="" id="inputSub" placeholder="Subjek">
											</div>
										</div>
										<div class="form-group">
											<label for="inputPesan" class="col-sm-2 control-label">Pesan</label>
											<div class="col-sm-10">
												<textarea name="pesan" class="form-control" id="inputPesan" placeholder="Pesan"></textarea>
											</div>
										</div>
										<br>
										<div class="form-group">
											<div class="col-sm-12 text-right">
												<input type="submit" class="btn btn-sm btn-fail" value="Kirim">
											</div>
										</div>
									</form>
									</div>
								</section>
							</article>
						</div>
						<!-- end of news -->

						<!-- Sidebar / iklan -->
						<div class="col-md-4">
							<section class="box">
							<?php
							if(!empty($data_iklan)):
								foreach ($data_iklan as $data):
							?>
								<a href="<?php echo $data['url'];?>" class="image">
									<img src="<?php echo $asset;?>upload/<?php echo $data['photo'];?>" alt="" />
								</a>
							<?php
								endforeach;
							endif;
							?>
							</section>
						</div>
						<!-- end of sidebar -->
					</div>	
				</div>
			</div>