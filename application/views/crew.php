				</div>
			</div>
			<!-- End Header -->

			<!-- News -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<!-- news -->
						<div class="col-md-8">
							<article class="box post">
								<!-- <a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a> -->
								<header>
									<h2>Crew</h2>
									<!-- <p>Eltras Radio Politeknik Negeri Bandung</p> -->
								</header>

								<?php if(!empty($data)): ?>
									<?php foreach ($data as $data): ?>
								<div class="crew-dk">
									<img src="<?php echo $data['foto'];?>" width="193" height="198" />
									<h5><?php echo $data['nama'];?> (<?php echo $data['jabatan']['name'];?>)</h5>
									<p><?php echo $data['description'];?></p>
									<button onclick="view(<?php echo $data['id'];?>)" class="btn btn-sm btn-fail">
										View Profile
									</button>
					            </div>
					            	<?php endforeach;?>
					            <?php endif;?>

					            <!-- <div class="box-footer clearfix">
					            	<ul class="pagination pagination-sm no-margin pull-right">
					            		<li class="disabled">
					            			<a href="#">1</a>
					            		</li>
					            		<li>
					            			<a href="#">2</a>
					            		</li>
					            		<li>
					            			<a href="#" rel="next">Lanjut &raquo;</a>
					            		</li>
					            	</ul>
					            </div> -->
					            <?php echo $halaman;?>
							</article>
						</div>
						<!-- end of news -->

						<!-- Sidebar / iklan -->
						<div class="col-md-4">
							<section class="box">
							<?php
							if(!empty($data_iklan)):
								foreach ($data_iklan as $data):
							?>
								<a href="<?php echo $data['url'];?>" class="image">
									<img src="<?php echo $asset;?>upload/<?php echo $data['photo'];?>" alt="" />
								</a>
							<?php
								endforeach;
							endif;
							?>
							</section>
						</div>
						<!-- end of sidebar -->
					</div>	
				</div>
			</div>

<script type="text/javascript">
	function view(obj){
		var id = obj;

		console.log(id);

		$.ajax({
			url:"http://localhost/eltras/crew/ambilSatu/"+id,
			type:'get',
			dataType: 'json',
			success: function(res) {
				console.log(res);
				$("#nama").val(res.data.name);
				if(res.data.isShow == 1) {
					$("#status").val("Aktif");
				} else {
					$("#status").val("Tidak Aktif");
				}
				$("#deskripsi").val(res.data.description);
				$("#prodi").val(res.data.prodi);
				$("#jurusan").val(res.data.jurusan);
				$("#angkatan").val(res.data.angkatan);
				$("#jabatan").val(res.data.jabatan);
			}
		});
		$('#modalDetail').modal('show'); // show bootstrap modal
	}
</script>

<div id="modalDetail" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="smaller lighter blue no-margin">Lihat Crew</h3>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<strong>Nama</strong>
						<input type="text" id="nama" disabled readonly="true" class="form-control" value="" />
					</div>
					<div class="col-md-6">
						<strong>Angkatan</strong>
						<input type="text" id="angkatan" disabled readonly="true" class="form-control" value="" />
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<strong>Deskripsi</strong>
						<textarea id="deskripsi" disabled readonly="true" class="form-control"></textarea>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<strong>Prodi</strong>
						<input type="text" id="prodi" disabled readonly="true" class="form-control" value="" />
					</div>
					<div class="col-md-6">
						<strong>Jurusan</strong>
						<input type="text" id="jurusan" disabled readonly="true" class="form-control" value="" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<strong>Jabatan</strong>
						<input type="text" id="jabatan" disabled readonly="true" class="form-control" value="" />
					</div>
					<div class="col-md-6">
						<strong>Status</strong>
						<input type="text" id="status" disabled readonly="true" class="form-control" value="" />
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>