
				</div>
			</div>
			
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<!-- news -->
						<div class="col-md-8">
							<article class="box post">
								<a href="#" class="image featured">
									<img src="<?php echo (!empty($datana['banner'])) ? base_url("assets/upload/".$datana['banner']) : base_url("assets/upload/default_banner.jpg"); ?>" alt="" />
								</a>
								<header>
									<h2><?php echo (!empty($datana['nama_page'])) ? $datana['nama_page'] : '-'; ?></h2>
									<p><?php echo (!empty($datana['deskripsi_page'])) ? $datana['deskripsi_page'] : '-'; ?></p>
								</header>
								<section>
									<?php echo (!empty($datana['content_page'])) ? htmlspecialchars_decode($datana['content_page']) : '-'; ?>
								</section>
							</article>
						</div>
						<!-- end of news -->

						<!-- Sidebar / iklan -->
						<div class="col-md-4">
							<section class="box">
							<?php
							if(!empty($data_iklan)):
								foreach ($data_iklan as $data):
							?>
								<a href="<?php echo $data['url'];?>" class="image">
									<img src="<?php echo $asset;?>upload/<?php echo $data['photo'];?>" alt="" />
								</a>
							<?php
								endforeach;
							endif;
							?>
							</section>
						</div>
						<!-- end of sidebar -->
					</div>	
				</div>
			</div>