<!doctype html>

<html lang="en-gb" class="no-js">
	<head>
		<meta charset="utf-8">
		<title><?php echo $_SESSION['site_name'];?></title>
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="description" content="">		

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!--Favicons -->
		<link rel="shortcut icon" href="<?php echo $asset;?>images/favicon.png">     

		<!--styles -->
		<link href="<?php echo $asset;?>css/bootstrap/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo $asset;?>css/fontawesome/font-awesome.css" rel="stylesheet">
		<link href="<?php echo $asset;?>js/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="<?php echo $asset;?>js/owl-carousel/owl.theme.css" rel="stylesheet">
		<link href="<?php echo $asset;?>js/owl-carousel/owl.transitions.css" rel="stylesheet">
		<link href="<?php echo $asset;?>css/magnific-popup.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo $asset;?>css/animate.css" />
		<link rel="stylesheet" href="<?php echo $asset;?>css/et-linefont/etlinefont.css">
		<link href="<?php echo $asset;?>css/style.css" type="text/css"  rel="stylesheet"/>


		<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
		<![endif]-->

		<body  data-spy="scroll" data-target="#main-menu">
 

		<!--Start Page loader -->
		<!-- <div id="pageloader">   
			<div class="loader">
				<img src="<?php echo $asset;?>images/progress.gif" alt='loader' />
			</div>
		</div> -->
		<!--End Page loader -->