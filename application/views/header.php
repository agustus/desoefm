
	<!--Start Navigation-->
	<header id="header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu">
								<span class="sr-only">Toggle navigation</span>
								<span class="fa fa-bars"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
                            <!--Start Logo -->
							<div class="logo-nav" style="padding: 5px;">
								<a href="<?php echo site_url();?>" style="color: #fff; font-size: 25px; font-weight: bold;text-transform: uppercase;">
									<!-- <img src="<?php echo $asset;?>images/logo.png" alt="<?php echo $_SESSION['site_name'];?>" /> -->
									<?php echo $_SESSION['site_name'];?>
								</a>
							</div>
                            <!--End Logo -->
							<div class="clear-toggle"></div>
							<div id="main-menu" class="collapse scroll navbar-right">
								<ul class="nav">
                                
									<li class="active"> <a href="#home">Home</a> </li>
									
									<li> <a href="#about">About</a> </li>
                                    
                                    <li> <a href="#history">History</a> </li>

                                    <li> <a href="#program">Program</a> </li>

                                    <li> <a href="#news">News</a> </li>

                                    <li> <a href="#galery">Galery</a> </li>
                                    
                                    <!-- <li> <a href="#works">Our Work</a> </li> -->

                                    <li> <a href="#blog">Blog</a></li>

                                    <li> <a href="#team">Team</a> </li>
                                   
								    <!-- <li> <a href="#services">Services</a> </li> -->
                                    
                                    <!-- <li> <a href="#testimonials">Testimonials</a></li> -->
                                     
									<li> <a href="#contact">Contact</a> </li>
										
								</ul>
							</div><!-- end main-menu -->
						</div>
					</div>
				</div>
			</header>
    <!--End Navigation-->
