<style type="text/css">
    .listJadwal {
        color: #000;
        margin-bottom: 10px;
    }
</style>

        <!-- Start Slider  -->
        <section id="home" class="home">
             <div class="slider-overlay"></div>
            <div class="flexslider">
                <ul class="slides scroll">
                    <li class="first">
                        <div class="slider-text-wrapper">  
                            <div class="container">
                                <div class="big">Staging Website DesoeFm.com</div>          
                                <div class="small">Are you ready to buy this theme</div>
                                <a href="#about" class="middle btn btn-white">VIEW PORTFOLIO</a>
                            </div>       
                        </div>
                        <img src="<?php echo $asset;?>images/slider/1.jpg" alt="">
                    </li>
                    
                    <li class="secondary">
                        <div class="slider-text-wrapper"> 
                            <div class="container">                       
                                <div class="big">Staging Website DesoeFm.com</div>          
                                <div class="small">Are you ready to buy this theme</div>
                                <a href="#about" class=" middle btn btn-white">VIEW PORTFOLIO</a>
                            </div>
                         </div>
                        <img src="<?php echo $asset;?>images/slider/2.jpg" alt="">
                    </li>
                    
                    <li class="third">
                        <div class="slider-text-wrapper"> 
                            <div class="container">                              
                                <div class="big">Staging Website DesoeFm.com</div>          
                                <div class="small">Are you ready to buy this theme</div>
                                <a href="#about" class="middle btn btn-white">VIEW PORTFOLIO</a>
                             </div>
                        </div>
                        <img src="<?php echo $asset;?>images/slider/3.jpg" alt="">
                    </li>
                </ul>
            </div>
        </section>
          <!-- End Slider  -->


		<!--Start Features-->
		<section  id="about" class="section">
			<div class="container">

				<div class="title-box text-center black">
					<h2 class="title">About</h2>
				</div>

				<div class="row">
					<div class="col-md-12">
						<?php echo @$about['content_page'];?>
					</div>
				</div> <!-- /.row-->
			</div> <!-- /.container-->
		</section> 
		<!--End Features-->


		<!--Start History-->
		<section id="history" class="section parallax" style="background: url(<?php echo base_url('assets/upload/'.$history['banner']);?>);">
			<div class="overlay"></div>

			<div class="container">
				<div class="title-box text-center white">
					<h2 class="title">History</h2>
				</div>

				<div class="row" style="background: rgba(255, 255, 255, .7); padding: 15px; color: #fff;">
					<div class="col-md-12 white">
						<?php echo @$history['content_page'];?>
					</div>
				</div>
			</div> <!--/.container-->
		</section>
		<!--End History-->


		<!--Start Program-->
		<section id="program" class="section">

			<div class="container">
				<div class="title-box text-center black">
					<h2 class="title">Program</h2>
				</div>

				<div class="row" style="background: rgba(255, 255, 255, .7); padding: 15px; color: #fff;">

                    <div class="col-md-7 white">
                        <div class="text-center black">
                            <h4>Jadwal Siaran</h4>
                        </div>
						<div class="tabs tabs-main">
                            <ul class="nav nav-tabs">
                                <?php 
                                    $nameOfDay = strtolower(date('D', strtotime(date('Y-m-d'))));
                                    $dayId = [
                                        'mon'   => 'Senin',
                                        'tue'   => 'Selasa',
                                        'wed'   => 'Rabu',
                                        'thu'   => 'Kamis',
                                        'fri'   => "Jum'at",
                                        'sat'   => 'Sabtu',
                                        'sun'   => 'Minggu'
                                    ];
                                ?>
                                <?php foreach ($jadwalSiaran as $day): ?>
                                <li <?php echo ($dayId[$nameOfDay]==$day['day']) ? 'class="active"' : ''; ?>>
                                    <a href="#hari<?php echo str_replace("'", "", ucwords($day['day']));?>" data-toggle="tab"><?php echo $day['day'];?></a>
                                </li>
                                <?php endforeach;?>
                            </ul>
                            <div class="tab-content">

                                <?php foreach ($jadwalSiaran as $day): ?>
                                <!--Start Tab Item -->
                                <div class="tab-pane in <?php echo ($dayId[$nameOfDay]==$day['day']) ? 'active' : ''; ?>" id="hari<?php echo str_replace("'", "", ucwords($day['day']));?>">
                                    <ul>
                                        <?php
                                        if(!empty($day['program'])):
                                            foreach ($day['program'] as $program):
                                        ?>
                                        <li class="listJadwal">
                                            <?php echo substr($program['time'], 0, 5);?> <?php echo $program['event'];?>
                                        </li>
                                        <?php
                                            endforeach;
                                        ?>
                                        <?php else:?>
                                        <strong style="color:#000;">Tidak ada jadwal siaran.</strong>
                                        <?php
                                        endif;
                                        ?>
                                    </ul>
                                </div>
                                <!-- End Tab -->
                                <?php endforeach;?>

                            </div>
                        </div>
					</div>

                    <div class="col-md-5 white">
                        <div class="text-center black">
                            <h4>Planning</h4>
                        </div>

                        <!-- <p>Nothing planning.</p> -->

                        <div class="panel-group accordion-main" id="accordion">

                            <?php 
                            if(!empty($planning)):
                                $no=1; 
                            ?>
                                <?php foreach ($planning as $pln): ?>

                            <!--About accordion #1-->
                            <div class="panel">
                                <div class="panel-heading collapsed" data-toggle="collapse"
                                data-parent="#accordion" data-target="#collapse<?php echo $no;?>">
                                    <h6 class="panel-title accordion-toggle">
                                        <?php echo $pln['planning_name'];?>
                                    </h6>
                                </div>
                                <div id="collapse<?php echo $no;?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                        <?php echo $pln['planning_description'];?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                                <?php 
                                $no++;
                                endforeach;
                                ?>
                            <?php else: ?>
                            <div class="text-center" style="color:black; margin-top: 20px;">
                                Tidak ada planning.
                            </div>
                            <?php endif;?>
                            
                        </div>
                    </div>

				</div>
			</div> 
            
		</section> 
		<!--End Program-->

        <!--Start News-->
        <section id="news" class="section parallax" style="background: url(<?php echo base_url('assets/upload/'.$history['banner']);?>);">
            <div class="overlay"></div>

            <div class="container">
                <div class="title-box text-center white">
                    <h2 class="title">News</h2>
                </div>

                <div class="row">

                    <div class="col-md-3">
                        <div class="blog-post">
                            <div class="post-media">
                                <img src="<?php echo $asset;?>images/blog/blog1.jpg" alt="">
                            </div>

                            <div class="post-desc">
                                <h4>consectetur adipisicing Inventore</h4>
                                <h5>12 May, 2015</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolorum, fugiat, eligendi magni quibusdam iure cupiditate ex voluptas unde
                                </p>
                                <a href="news.html" class="btn btn-gray-border">Read More</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="blog-post">
                            <div class="post-media">
                                <img src="<?php echo $asset;?>images/blog/blog1.jpg" alt="">
                            </div>

                            <div class="post-desc">
                                <h4>consectetur adipisicing Inventore</h4>
                                <h5>12 May, 2015</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolorum, fugiat, eligendi magni quibusdam iure cupiditate ex voluptas unde
                                </p>
                                <a href="news.html" class="btn btn-gray-border">Read More</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="blog-post">
                            <div class="post-media">
                                <img src="<?php echo $asset;?>images/blog/blog1.jpg" alt="">
                            </div>

                            <div class="post-desc">
                                <h4>consectetur adipisicing Inventore</h4>
                                <h5>12 May, 2015</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolorum, fugiat, eligendi magni quibusdam iure cupiditate ex voluptas unde
                                </p>
                                <a href="news.html" class="btn btn-gray-border">Read More</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="blog-post">
                            <div class="post-media">
                                <img src="<?php echo $asset;?>images/blog/blog1.jpg" alt="">
                            </div>

                            <div class="post-desc">
                                <h4>consectetur adipisicing Inventore</h4>
                                <h5>12 May, 2015</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolorum, fugiat, eligendi magni quibusdam iure cupiditate ex voluptas unde
                                </p>
                                <a href="news.html" class="btn btn-gray-border">Read More</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!--/.container-->
        </section>
        <!--End News-->

        <!--Start galery-->
        <section id="galery" class="section">
            <!-- Filtering -->
            <div class="title-box text-center">
                <h2 class="title black">Galery</h2>
            </div>

            <div class=" col-md-12 text-center">
				<!-- Filtering -->
				<ul class="filtering">
					<li class="filter" data-filter="all">All</li>
					<li class="filter" data-filter="event">Events</li>
                    <li class="filter" data-filter="fashion">Social</li>
				</ul>
            </div>

            <div class="work-main">
					<!-- Work Grid -->
					<ul class="work-grid">
						<!-- Work Item -->
						<li class="work-item thumnail-img mix corporate">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img1.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img1.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->
                        
						<!-- Work Item -->
						<li class="work-item thumnail-img mix fashion wedding">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img2.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img2.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->
                        
						<!-- Work Item -->
						<li class="work-item thumnail-img mix corporate">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img3.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                   <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img3.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->

						<!-- Work Item -->
						<li class="work-item thumnail-img mix corporate">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img4.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img4.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->
                        
						<!-- Work Item -->
						<li class="work-item thumnail-img mix fashion wedding">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img5.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img5.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->
						
						<!-- Work Item -->
						<li class="work-item thumnail-img mix event wedding">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img6.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img6.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->

						<!-- Work Item -->
						<li class="work-item thumnail-img mix event">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img7.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img7.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->

						<!-- Work Item -->
						<li class="work-item thumnail-img mix corporate">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img8.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img8.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->

						<!-- Work Item -->
						<li class="work-item thumnail-img mix event">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img9.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img9.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->

						<!-- Work Item -->
						<li class="work-item thumnail-img mix wedding">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img10.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img10.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->

						<!-- Work Item -->
						<li class="work-item thumnail-img mix fashion">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img11.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img11.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->

						<!-- Work Item -->
						<li class="work-item thumnail-img mix corporate">
							<div class="work-image">
                                <img src="<?php echo $asset;?>images/works/img12.jpg" alt="thumbnail">
                                
                                 <!--Hover link-->
                                 <div class="hover-link">
                                    <a href="single-work.html">
                                        <i class="fa fa-link"></i>
                                    </a>
        
                                    <a href="<?php echo $asset;?>images/works/img12.jpg" class="popup-image">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                 </div>
                                 <!--End link-->

                                 <!--Hover Caption-->
                                 <div class="work-caption">
                                     <h4>Project Title</h4>
                                     <p>Photography</p>
                                 </div>
                                 <!--End Caption-->

                            </div> <!-- /.work-image-->
						</li> 
						<!--End Work Item -->
					</ul>
                    
				</div>
			</section>
  <!--End galery-->

        <!--Start Blog-->
        <section id="blog" class="section parallax" style="background: url(<?php echo base_url('assets/upload/'.$history['banner']);?>);">
            <div class="overlay"></div>

            <div class="container">
                <div class="title-box text-center white">
                    <h2 class="title">Blog</h2>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <div class="blog-post">
                            <div class="post-media">
                                <img src="<?php echo $asset;?>images/blog/blog1.jpg" alt="">
                            </div>

                            <div class="post-desc">
                                <h4>consectetur adipisicing Inventore</h4>
                                <h5>12 May, 2015</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolorum, fugiat, eligendi magni quibusdam iure cupiditate ex voluptas unde
                                </p>
                                <a href="blog.html" class="btn btn-gray-border">Read More</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="blog-post">
                            <div class="post-media">
                                <img src="<?php echo $asset;?>images/blog/blog1.jpg" alt="">
                            </div>

                            <div class="post-desc">
                                <h4>consectetur adipisicing Inventore</h4>
                                <h5>12 May, 2015</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolorum, fugiat, eligendi magni quibusdam iure cupiditate ex voluptas unde
                                </p>
                                <a href="blog.html" class="btn btn-gray-border">Read More</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="blog-post">
                            <div class="post-media">
                                <img src="<?php echo $asset;?>images/blog/blog1.jpg" alt="">
                            </div>

                            <div class="post-desc">
                                <h4>consectetur adipisicing Inventore</h4>
                                <h5>12 May, 2015</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolorum, fugiat, eligendi magni quibusdam iure cupiditate ex voluptas unde
                                </p>
                                <a href="blog.html" class="btn btn-gray-border">Read More</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!--/.container-->
        </section>
        <!--End Blog-->
 
 
   <!--Start Call To Action-->
   <!-- <section id="cta" class="parallax">
       <div class="overlay"></div>
         <div class="container">
             <div class="row text-center">
                 <h2>Are you ready to buy this theme</h2>
                 <a href="#" class="btn btn-green">GET A FREE QUOTE</a>
           </div>
         </div>
   </section> -->
   <!--End Call To Action-->
   
   
    
  <!--Start Team-->
  <section id="team" class="section">
		<div class="container">
            <div class="row">
                    
             <div class="title-box text-center">
                <h2 class="title">Team</h2>
             </div>
              
             </div>
			
                <!-- Team -->
				<div class="team-items team-carousel">

                <?php $no = 1; ?>
                    <?php foreach ($crew as $crew): ?>
                        <!-- Team Member #<?php echo $no;?> -->
                      <div class="item">
                          <img width="380px" src="<?php echo base_url('assets/upload/'.$crew['photo']);?>" alt=""  />
                          <h4><?php echo $crew['name'];?></h4>
                          <h5><?php echo $crew['jabatan'];?></h5>
                          <p><?php echo htmlspecialchars_decode($crew['description']); ?></p>
                          
                          <div class="socials">
                            <ul>
                            <?php if(!empty($crew['facebook'])): ?> 
                               <li><a class="facebook" href="<?php echo $crew['facebook'];?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <?php endif;?>

                            <?php if(!empty($crew['twitter'])): ?> 
                               <li><a class="twitter" href="<?php echo $crew['twitter'];?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <?php endif;?>

                            <?php if(!empty($crew['linkedin'])): ?> 
                               <li><a class="linkedin" href="<?php echo $crew['linkedin'];?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <?php endif;?>

                            <?php if(!empty($crew['gplus'])): ?> 
                               <li><a class="google-plus" href="<?php echo $crew['gplus'];?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <?php endif;?>
                            </ul>
                          </div>
                      </div>
                      <!-- End Member -->

                    <?php endforeach;?>

				</div>
                <!-- End Team -->
			</div>
            <!-- End Content -->
	</section>
  <!--End Team-->
   
   <!--Start Contact-->
   <section id="contact" class="section parallax">
      <div class="overlay"></div>
       <div class="container">
           <div class="row">
           
                 <div class="title-box text-center white">
                    <h2 class="title">Contact</h2>
                 </div>
             </div>

               <!--Start contact form-->
               <div class="col-md-8 col-md-offset-2 contact-form">
               
                    <div class="contact-info text-center">
                       <p><?php echo $_SESSION['site_phone'];?></p>
                       <p><?php echo $_SESSION['site_address'];?></p>
                       <p><?php echo $_SESSION['site_email_server'];?></p>
                    </div>
                     
                     <form method="post">
                        <div class="row">
                            <div class="col-md-4">
                                <input class="form-control" id="name" placeholder="Full Name" type="text">
                            </div>
                            <div class="col-md-4">
                                <input class="form-control" id="email" placeholder="Your Email" type="email">
                            </div>
                            <div class="col-md-4">
                                <input class="form-control" id="subject" placeholder="Subject" type="text">
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control" id="message" rows="7" placeholder="Your Message"></textarea>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-green">SEND MESSAGE</button>
                            </div>
                        </div>
                    </form>
               </div>
              <!--End contact form-->
    