<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_chart extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:27:11
	**/

	function __construct(){
		parent::__construct();
		$this->top_chart = 'top_chart';
		$this->music = 'music';
	}

	public function getAll(){
		$query = $this->db->select($this->top_chart.".*, ".$this->music.".title ,".$this->music.".artist ,".$this->music.".album ,".$this->music.".description ,".$this->music.".link")->order_by($this->top_chart.".position",'asc')->join($this->music, $this->music.".id = ".$this->top_chart.".music_id")->get($this->top_chart);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir=""){
		$query = $this->db->select($this->top_chart.".*, ".$this->music.".title ,".$this->music.".artist ,".$this->music.".album ,".$this->music.".description ,".$this->music.".link")->order_by($this->top_chart.".position",'asc')->join($this->music, $this->music.".id = ".$this->top_chart.".music_id")->get($this->top_chart,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($where){
		$query = $this->db->select($this->top_chart.".*, ".$this->music.".title ,".$this->music.".artist ,".$this->music.".album ,".$this->music.".description ,".$this->music.".link")->order_by($this->top_chart.".position",'asc')->join($this->music, $this->music.".id = ".$this->top_chart.".music_id")->get_where($this->top_chart,$where);
		
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function change($id=null,$ubah=array()){
		$query = $this->db->update($this->top_chart, $ubah, array('id'=>$id));

		return $query;
	}

	public function add($field=array()){
		$query = $this->db->insert($this->top_chart, $field);
		
		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('top_chart.id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->top_chart,array('id' => $id)) : false;
	}

	public function getMax(){
		$query = $this->db->select_max('position')->get($this->top_chart);
		$query = $query->result_array();

		return $query;
	}
}