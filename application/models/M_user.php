<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_user extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:27:11
	**/

	function __construct(){
		parent::__construct();
		$this->user = 'user';
	}

	public function getAll(){
		$query = $this->db->where_not_in('username', $this->session->userdata('uname'));
		$query = $this->db->get($this->user);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir=""){
		$query = $this->db->where_not_in('username', $this->session->userdata('uname'));
		$query = $this->db->get($this->user,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($where=array()){
		$query = $this->db->get_where($this->user,$where);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function change($id=null,$ubah=array()){
		$query = $this->db->update($this->user, $ubah, array('id'=>$id));

		return $query;
	}

	public function add($field=array()){
		$query = $this->db->insert($this->user, $field);

		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->user,array('id' => $id)) : false;
	}
}