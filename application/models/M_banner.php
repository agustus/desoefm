<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_banner extends CI_Model {

	/**
		* @Author				: ferdhika
		* @Filename 			: M_banner.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-25 18:13:20
	**/

	function __construct(){
		parent::__construct();
		$this->banner = 'banner';
	}

	public function getAll($where=array()){
		$query = $this->db->get_where($this->banner, $where);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir="",$where=array()){
		$query = $this->db->where($where);
		$query = $this->db->get($this->banner,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($kondisi=array()){
		$query = $this->db->where($kondisi);
		$query = $this->db->get($this->banner);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function add($data=array()){
		$query = $this->db->insert($this->banner,$data);
		$query = $this->db->insert_id();
		return $query;
	}

	public function edit($data=array(),$idna=array()){
		$query = $this->db->update($this->banner,$data,$idna);
		return $query;	
	}

	public function remove($where=array()){
		$query = $this->db->delete($this->banner,$where);
		return $query;
	}

}