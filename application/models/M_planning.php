<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_planning extends CI_Model {

	/**
		* @Author				: ferdhika
		* @Filename 			: M_planning.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-07-31 21:30:26
	**/

	function __construct(){
		parent::__construct();
		$this->tb = 'planning';
	}

	public function getAll($where=array()){
		if(!empty($where)){
			$query = $this->db->where($where);
		}
		$query = $this->db->get($this->tb);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir=""){
		$query = $this->db->get($this->tb,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($where=array()){
		$query = $this->db->get_where($this->tb,$where);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function change($id=null,$ubah=array()){
		$query = $this->db->update($this->tb, $ubah, array('id'=>$id));

		return $query;
	}

	public function add($field=array()){
		$query = $this->db->insert($this->tb, $field);

		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->tb,array('id' => $id)) : false;
	}
}