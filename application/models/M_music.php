<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_music extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:27:11
	**/

	function __construct(){
		parent::__construct();
		$this->music = 'music';
	}

	public function getAll(){
		$query = $this->db->get($this->music);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir=""){
		$query = $this->db->get($this->music,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($where){
		$query = $this->db->get_where($this->music,$where);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function change($id=null,$ubah=array()){
		$query = $this->db->update($this->music, $ubah, array('id'=>$id));

		return $query;
	}

	public function add($field=array()){
		$query = $this->db->insert($this->music, $field);

		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->music,array('id' => $id)) : false;
	}

}