<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_news extends CI_Model {

	/**
		* @Author				: Ferdhika Yudira
		* @Filename 			: M_news.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-06 15:40:40
	**/

	function __construct(){
		parent::__construct();
		$this->article = 'post';
	}

	public function getAll($where=array()){
		$query = $this->db->get_where($this->article, $where);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir="",$where=array()){
		$query = $this->db->order_by("date_created",'desc');
		$query = $this->db->where($where);
		$query = $this->db->get($this->article,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($kondisi=array()){
		$query = $this->db->where($kondisi);
		$query = $this->db->get($this->article);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function add($data=array()){
		$query = $this->db->insert($this->article,$data);
		$query = $this->db->insert_id();
		return $query;
	}

	public function edit($data=array(),$idna=array()){
		$query = $this->db->update($this->article,$data,$idna);
		return $query;	
	}

	public function remove($where=array()){
		$query = $this->db->delete($this->article,$where);
		return $query;
	}

}