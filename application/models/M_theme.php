<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_theme extends CI_Model {

	/**
		* @Author				: ferdhika
		* @Filename 			: M_theme.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-25 14:43:40
	**/

	function __construct(){
		parent::__construct();
		$this->theme = 'theme';
	}

	public function getAll($where=array()){
		$query = $this->db->get_where($this->theme, $where);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir="",$where=array()){
		$query = $this->db->order_by("tahun",'desc');
		$query = $this->db->order_by("bulan",'desc');
		$query = $this->db->where($where);
		$query = $this->db->get($this->theme,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($kondisi=array()){
		$query = $this->db->where($kondisi);
		$query = $this->db->get($this->theme);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function add($data=array()){
		$query = $this->db->insert($this->theme,$data);
		$query = $this->db->insert_id();
		return $query;
	}

	public function edit($data=array(),$idna=array()){
		$query = $this->db->update($this->theme,$data,$idna);
		return $query;	
	}

	public function remove($where=array()){
		$query = $this->db->delete($this->theme,$where);
		return $query;
	}

}