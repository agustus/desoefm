<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_program extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:27:11
	**/

	function __construct(){
		parent::__construct();
		$this->program = 'program';
		$this->day = 'day';
	}

	public function getAllperDay($id){
		
		$query = $this->db->order_by('time','asc')->get_where($this->program,['day_id'=>$id,'status'=>1]);
        $query = $query->result_array();

        return $query;
	}

	public function getAllday(){
		$query = $this->db->get_where($this->day,['status'=>1]);
        $query = $query->result_array();

        return $query;
	}

	public function getOneday($where=array()){
		$query = $this->db->get_where($this->day,$where);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function getOne($where=array()){
		$query = $this->db->get_where($this->program,$where);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function add($field=array()){
		$query = $this->db->insert($this->program, $field);

		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->program,array('id' => $id)) : false;
	}
}