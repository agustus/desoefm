<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_auth extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-04 21:17:34
	**/

	function __construct(){
		parent::__construct();
		$this->user = 'user';
	}

	public function masuk($login=array()){
		// $query = $this->db->get_where($this->user,array('username'=>$login['username'],'password'=>crypt($login['password'], KEY_PASS) ));
		$query = $this->db->get_where($this->user,array('username'=>$login['username'],'password'=>md5($login['password']) ));
		$query = $query->result_array();
		
		if($query){
			return $query[0];	
		}
	}

	public function ambilSatuUser($where=array()){
		$query = $this->db->get_where($this->user,$where);
		$query = $query->result_array();
		
		if($query){
			return $query[0];	
		}
	}
}