<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_iklan extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:27:11
	**/

	function __construct(){
		parent::__construct();
		$this->iklan = 'iklan';
	}

	public function getAll(){
		$query = $this->db->get($this->iklan);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir=""){
		$query = $this->db->get($this->iklan,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($where){
		$query = $this->db->get_where($this->iklan,$where);
		
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function change($id=null,$ubah=array()){
		$query = $this->db->update($this->iklan, $ubah, array('id'=>$id));

		return $query;
	}

	public function add($field=array()){
		$query = $this->db->insert($this->iklan, $field);

		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->iklan,array('id' => $id)) : false;
	}
	
}