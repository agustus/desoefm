<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_crew extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:27:11
	**/

	function __construct(){
		parent::__construct();
		$this->crew = 'crew';
		$this->jurusan = 'jurusan';
		$this->prodi = 'prodi';
		$this->jabatan = 'jabatan';
	}

	public function getAll($where=array()){
		if(!empty($where)){
			$query = $this->db->where($where);
		}
		$query = $this->db->select($this->jabatan.".name AS jabatan, ".$this->crew.".*")
			->from($this->crew)
			->join($this->jabatan,$this->jabatan.".id = ".$this->crew.".jabatan_id")->get();
		
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir=""){
		$query = $this->db->get($this->crew,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($where){
		$query = $this->db->select($this->jabatan.".name AS jabatan, ".$this->crew.".*")
		->from($this->crew)
		->join($this->jabatan,$this->jabatan.".id = ".$this->crew.".jabatan_id")
		->where($where)->get();
		
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function change($id=null,$ubah=array()){
		$query = $this->db->update($this->crew, $ubah, array('id'=>$id));

		return $query;
	}

	public function add($field=array()){
		$query = $this->db->insert($this->crew, $field);

		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('crew.id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->crew,array('id' => $id)) : false;
	}

	public function getProdi(){
		$query = $this->db->get($this->prodi);
        $query = $query->result_array();

        return $query;
	}

	public function getJabatan(){
		$query = $this->db->get($this->jabatan);
        $query = $query->result_array();

        return $query;
	}

	public function getOneJabatan($where=array()){
		$query = $this->db->get_where($this->jabatan,$where);
        $query = $query->result_array();

        if($query){
        	return $query[0];
        }
	}
}