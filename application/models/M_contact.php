<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_contact extends CI_Model {
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-05 10:27:11
	**/

	function __construct(){
		parent::__construct();
		$this->contact = 'contact_us';
	}

	public function getAll(){
		$query = $this->db->get($this->contact);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir=""){
		$query = $this->db->get($this->contact,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($where){
		$query = $this->db->get_where($this->contact,$where);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function change($id=null,$ubah=array()){
		$query = $this->db->update($this->contact, $ubah, array('id'=>$id));

		return $query;
	}

	public function add($field=array()){
		$query = $this->db->insert($this->contact, $field);

		return $query;
	}

	public function delete($id=0){
		$cek = $this->getOne(array('id'=>$id));

		return (!empty($cek)) ? $this->db->delete($this->contact,array('id' => $id)) : false;
	}

	public function getProdi(){
		$query = $this->db->get($this->prodi);
        $query = $query->result_array();

        return $query;
	}

	public function getJabatan(){
		$query = $this->db->get($this->jabatan);
        $query = $query->result_array();

        return $query;
	}

	public function getOneJabatan($where=array()){
		$query = $this->db->get_where($this->jabatan,$where);
        $query = $query->result_array();

        if($query){
        	return $query[0];
        }
	}
}