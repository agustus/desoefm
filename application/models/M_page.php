<?php if ( ! defined('BASEPATH')) exit('Alag siah!');

class M_page extends CI_Model {

	/**
		* @Author				: ferdhika
		* @Filename 			: M_page.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-25 13:54:51
	**/

	function __construct(){
		parent::__construct();
		$this->page = 'page';
	}

	public function getAll($where=array()){
		$query = $this->db->get_where($this->page, $where);
        $query = $query->result_array();

        return $query;
	}

	public function getAllPer($awal="",$akhir="",$where=array()){
		$query = $this->db->where($where);
		$query = $this->db->get($this->page,$awal,$akhir);
        $query = $query->result_array();

        return $query;
	}

	public function getOne($kondisi=array()){
		$query = $this->db->where($kondisi);
		$query = $this->db->get($this->page);
        $query = $query->result_array();

        if(!empty($query)){
        	return $query[0];
        }
	}

	public function add($data=array()){
		$query = $this->db->insert($this->page,$data);
		$query = $this->db->insert_id();
		return $query;
	}

	public function edit($data=array(),$idna=array()){
		$query = $this->db->update($this->page,$data,$idna);
		return $query;	
	}

	public function remove($where=array()){
		$query = $this->db->delete($this->page,$where);
		return $query;
	}

}