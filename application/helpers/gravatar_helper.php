<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-08-28 16:17:27
	**/

	if(!function_exists('gravatar')){
		function gravatar($email,$ukuran=200,$type="pg"){
			return "https://s.gravatar.com/avatar/".md5($email)."?s=".$ukuran."&r=".$type;
		}
	}