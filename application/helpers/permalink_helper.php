<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2015-07-07 21:31:18
	**/
	
	if ( ! function_exists('set_permalink')) {

		function set_permalink($id,$content) {
			$karakter = array ('{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','-','/','\\',',','.','#',':',';','\'','"','[',']');
			$hapus_karakter_aneh = strtolower(str_replace($karakter,"",$content));
			$tambah_strip = strtolower(str_replace(' ', '-', $hapus_karakter_aneh));
			$link_akhir = $id.'-'.$tambah_strip;
			// $link_akhir = $tambah_strip;
			return $link_akhir;
		}
	}

	if ( ! function_exists('set_slug')) {

		function set_slug($content) {
			$karakter = array ('{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','-','/','\\',',','.','#',':',';','\'','"','[',']');
			$hapus_karakter_aneh = strtolower(str_replace($karakter,"",$content));
			$tambah_strip = strtolower(str_replace(' ', '-', $hapus_karakter_aneh));
			$link_akhir = $tambah_strip;
			return $link_akhir;
		}
	}