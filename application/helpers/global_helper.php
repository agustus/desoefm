<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
		* @Author				: Ferdhika Yudira
		* @Filename 			: global_helper.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-01 14:18:33
	**/

	if ( ! function_exists('get_image')) {

		function get_image($text="") {
			preg_match_all('/<img[^>]+>/i',$text, $result); 

			for ($i = 0; $i < count($result[0]); $i++) {
				// get the source string
				preg_match('/src="([^"]+)/i',$result[0][$i], $imgage);

				// remove opening 'src=' tag, can`t get the regex right
				$origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
			}

			return $origImageSrc;
		}
	}

	if ( ! function_exists('get_cuplikan')) {

		function get_cuplikan($text="") {
			$text = explode('.', strip_tags($text));

			if(!empty($text)){
				$text = ltrim($text[0]);
			}

			return $text;
		}
	}