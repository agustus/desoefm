<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Radio extends Main { 
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-04 21:04:53
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array("m_page","m_iklan"));
	}

	public function index(){
		// Info halaman

		$this->global_data['datana'] = $this->m_page->getOne(array('type'=>'radio'));
		$this->global_data['data_iklan'] = $this->m_iklan->getAll();
		$this->global_data['active_menu'] = "radio";
		$this->tampilan('radio');
	}
}