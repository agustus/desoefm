<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Contact extends Main { 
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-04 21:04:53
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array('m_contact','m_iklan'));
		$this->load->library('form_validation');
	}

	public function index(){
		// Info halaman
		
		$this->global_data['active_menu'] = "contact";
		$this->global_data['data_iklan'] = $this->m_iklan->getAll();

		// Validasi
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('subjek', 'Subjek', 'trim|required');
		$this->form_validation->set_rules('pesan', 'Pesan', 'trim|required');

    	$data['nama'] = $this->input->post('nama');
    	$data['email'] = $this->input->post('email');
    	$data['subjek'] = $this->input->post('subjek');
    	$data['pesan'] = $this->input->post('pesan');

        if($this->form_validation->run()){

        	$field = array(
        		'name'			=> $data['nama'],
        		'email'			=> $data['email'],
        		'subject'		=> $data['subjek'],
        		'message'			=> $data['pesan'],
        		'date_created' 	=> date('Y-m-d h:i:s')
        	);

        	$tambah = $this->m_contact->add($field);

        	if($tambah){
        		$pesan = "<div class=\"alert alert-success alert-dismissable\">";
				$pesan .= "		<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
				$pesan .= "		<h4><i class=\"icon fa fa-check\"></i> Alert!</h4>";
				$pesan .= "		Berhasil menambah Crew.";
				$pesan .= "	</div>";
        		$this->session->set_flashdata('message',$pesan);
        		redirect('contact');
        	}else{
        		$this->session->set_flashdata('message',"Gagal. Kesalahan database.");
        		redirect('contact');
        	}
        }else{
			// Pesan validasi
			$this->global_data['datana'] = $data;
			$this->global_data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		}

		$this->tampilan('contact.php');
	}
}