<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Crew extends Main { 
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-04 21:04:53
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array('m_crew','m_iklan'));

		$this->load->library(['pagination']);	
	}

	public function page($id=0){
		$this->index($id);
	}

	public function index($id=0){
		// Info halaman
		$this->global_data['active_menu'] = "crew";
		$this->global_data['data_iklan'] = $this->m_iklan->getAll();

		// Pengaturan pagination
		$config['base_url'] = site_url('crew/page');
		$config['total_rows'] = count($this->m_crew->getAll());
		$config['per_page'] = $this->session->userdata('site_list_limit');
		$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul></div>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$crew = $this->m_crew->getAllPer($config['per_page'], $id);

		$this->global_data['data'] = array();

		$no=1+$id;
		foreach ($crew as $result) {
			$jabatan = $this->m_crew->getOneJabatan(array('id'=>$result['jabatan_id']));

			$this->global_data['data'][] = array(
				'no'			=> $no,
				'id'			=> $result['id'],
				'nama'			=> $result['name'],
				'jabatan'		=> $jabatan,
				'description'	=> $result['description'],
				'foto'			=> (!empty($result['photo'])) ? base_url('assets/upload/'.$result['photo']) : base_url("assets/img/anggota/default.png"),
				'isShow'		=> $result['isShow'],
				'date_created'	=> tgl_indo(substr($result['date_created'], 0, 10))
			);
			$no++;
		}

		$this->tampilan('crew');
	}

	public function ambilSatu($id=0){
		$json = array(
			'messages'	=> "Kosong!",
			'data' 		=> null,
			'status'	=> false
		);
		$data = $this->m_crew->getOne(array('crew.id'=>$id));

		if(!empty($data)){
			$json = array(
				'messages'	=> "Data ada!",
				'data' 		=> $data,
				'status'	=> true
			);
		}

		$this->outputJson($json);
	}
}