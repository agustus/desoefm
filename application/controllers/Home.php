<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Home extends Main { 

	/**
		* @Author				: Ferdhika Yudira
		* @Filename 			: Home.php
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2017-02-06 15:28:20
	**/

	function __construct(){
		parent::__construct();

		$this->load->model([
			'm_auth', 
			'm_theme', 
			'm_banner', 
			'm_page', 
			'm_crew', 
			'm_program',
			'm_planning'
		]);
	}

	public function index(){
		// Info halaman
		$this->global_data['active_menu'] = "home";

		// Theme
		$this->global_data['dataAktif'] = $this->m_theme->getOne(array('bulan'=> date('m'), 'tahun'=> date('Y')));

		// Banner
		$this->global_data['banner'] = $this->m_banner->getAll(array('aktif'=> 1));

		// Page About
		$this->global_data['about'] = $this->m_page->getOne(array('type'=> 'about'));

		// Page Program
		$allDay = $this->m_program->getAllday();

		$this->global_data['jadwalSiaran'] = array();

		foreach ($allDay as $allDay) {
			$program = $this->m_program->getOneday(['id' => $allDay['id']]);

			$this->global_data['jadwalSiaran'][] = [
				'id'	=> $allDay['id'],
				'day'	=> $allDay['day'],
				'program'=> $this->m_program->getAllperDay($allDay['id'])
			];
		}

		$this->global_data['planning'] = $this->m_planning->getAll(array('planning_status'=> true));

		// Page History
		$this->global_data['history'] = $this->m_page->getOne(array('type'=> 'history'));

		// Page Crew
		$this->global_data['crew'] = $this->m_crew->getAll(array('isShow'=> true));

		$this->tampilan('home');
	}
}