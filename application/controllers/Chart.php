<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class Chart extends Main { 
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-04 21:04:53
	**/

	function __construct(){
		parent::__construct();

		$this->load->model('');
	}

	public function index(){
		// Info halaman
		
		$this->global_data['active_menu'] = "chart";
		$this->tampilan('chart');
	}
}