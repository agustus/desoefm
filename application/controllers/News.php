<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Main.php");

class News extends Main { 
	
	/**
		* @Author				: Localhost {Ferdhika Yudira}
		* @Email				: fer@dika.web.id
		* @Web					: http://dika.web.id
		* @Date					: 2016-10-04 21:04:53
	**/

	function __construct(){
		parent::__construct();

		$this->load->model(array('m_iklan', 'm_news', 'm_user'));

		$this->load->library(['pagination']);

		$this->load->helper(['permalink','global']);
	}

	public function page($id=0){
		$this->index($id);
	}

	public function index($id=0){
		// Info halaman
		$this->global_data['active_menu'] = "news";
		$this->global_data['data_iklan'] = $this->m_iklan->getAll();

		// Pengaturan pagination
		$config['base_url'] = site_url().'news/page';
		$config['total_rows'] = count($this->m_news->getAll(array('status'=>1)));
		$config['per_page'] = $this->session->userdata('site_list_limit'); //
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['next_link'] = 'Lanjut &raquo;';
		$config['prev_link'] = '&laquo; Kembali';
		$config['cur_tag_open'] = '<li class="disabled"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_links'] = 1;
		$config['last_link'] = '<b>Akhir &rsaquo;</b>';
		$config['first_link'] = '<b>&lsaquo; Awal</b>';

		//inisialisasi config pagination
		$this->pagination->initialize($config);

		//buat pagination
		$this->global_data['halaman'] = $this->pagination->create_links();

		// data
		$datana = $this->m_news->getAllPer($config['per_page'], $id, array('status'=>1));

		$news = array();

		foreach ($datana as $res) {
			$user = $this->m_user->getOne(array('id'=> $res['user_id']));

			$news[] = array(
				'article_id'			=> $res['id'],
				'article_title'			=> $res['title'],
				'article_cuplikan'		=> get_cuplikan($res['content']).'.',
				'article_banner'		=> (!empty(get_image($res['content']))) ? get_image($res['content'])[0]: base_url('assets/upload/default_banner.jpg'),
				'article_content'		=> $res['content'],
				'date_created'			=> tgl_indo(substr($res['date_created'],0,10)),
				'nama_user'				=> ucwords($user['name']),
				'link_detail'			=> site_url('news/detail/'.set_permalink($res['id'],$res['title']))
			);
		}

		$this->global_data['news'] = $news;

		$this->tampilan('news/news');
	}

	public function detail(){
		$id = str_replace('-',' ', $this->uri->segment(3));

		(empty($id)) ? redirect('event') : '';

		$this->global_data['datana'] = $this->m_news->getOne(['id'=>$id, 'status'=> 1]);

		// Identitas halaman
		$this->global_data['active_menu'] = "news";
		$this->global_data['data_iklan'] = $this->m_iklan->getAll();
		$this->global_data['description'] = get_cuplikan($this->global_data['datana']['content']);
		$this->global_data['activeList'] = true;
		

		$this->tampilan('news/detail');
	}

}